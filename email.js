import emailjs from "@emailjs/browser";
import dotenv from "dotenv";

dotenv.config();

const sendEmailCF = async (vars) => {
  const service_id = process.env.EMAILJS_SERVICE_ID;
  const template_id = process.env.EMAILJS_TEMPLATE_ID_CF;
  const public_key = process.env.EMAILJS_PUBLIC_KEY;
  return () => {
    return emailjs.sendForm(service_id, template_id, vars, public_key);
  };
};
const sendEmailPRMe = async (vars) => {
  const service_id = process.env.EMAILJS_SERVICE_ID;
  const template_id = process.env.EMAILJS_TEMPLATE_ID_CF;
  const public_key = process.env.EMAILJS_PUBLIC_KEY;
  return () => {
    return emailjs.send(service_id, template_id, vars, public_key);
  };
};
const sendEmailPR = async (vars) => {
  const service_id = process.env.EMAILJS_SERVICE_ID;
  const template_id = process.env.EMAILJS_TEMPLATE_ID_PR;
  const public_key = process.env.EMAILJS_PUBLIC_KEY;
  return () => {
    return emailjs.send(service_id, template_id, vars, public_key);
  };
};

export { sendEmailCF, sendEmailPR, sendEmailPRMe };
