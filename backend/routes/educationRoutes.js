import express from "express";
import { getEducation } from "../controllers/educationController.js";
const router = express.Router();

router.route("/").get(getEducation);

export default router;
