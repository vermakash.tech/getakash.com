import express from "express";
import { createProjectRequest, getDescriptions, getTaglines } from "../controllers/initialController.js";
const router = express.Router();

router.route("/taglines").get(getTaglines);
router.route("/descriptions").get(getDescriptions);

router.route("/projectrequest").post(createProjectRequest);

export default router;
