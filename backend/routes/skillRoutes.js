import express from "express";
const router = express.Router();
import { getSkills, deleteSkill, createSkill, updateSkill } from "../controllers//skillController.js";

router.route("/").get(getSkills).post(createSkill);

router.route("/:id").delete(deleteSkill).put(updateSkill);

export default router;
