import express from "express";
import { createRecommendation, getRecommendations } from "../controllers/recommendationController.js";
const router = express.Router();

router.route("/").get(getRecommendations).post(createRecommendation);

export default router;
