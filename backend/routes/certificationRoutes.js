import express from "express";
import { getCertis } from "../controllers/certificationController.js";
const router = express.Router();

router.route("/").get(getCertis);

export default router;
