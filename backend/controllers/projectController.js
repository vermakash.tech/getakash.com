import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Project from "../models/projectModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all projects
// @route   GET /api/projects
// @access  Public
const getProjects = asyncHandler(async (req, res) => {
  const projects = await Project.find().sort({ _id: 1 });
  res.json({
    status: "success",
    results: projects.length,
    data: { projects },
  });
});

export { getProjects };
