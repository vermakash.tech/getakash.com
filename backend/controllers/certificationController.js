import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Certi from "../models/certificationModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all certifications
// @route   GET /api/certifications
// @access  Public
const getCertis = asyncHandler(async (req, res) => {
  const certis = await Certi.find();
  res.json({
    status: "success",
    results: certis.length,
    data: { certis },
  });
});

export { getCertis };
