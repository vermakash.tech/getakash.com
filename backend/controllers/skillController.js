import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Skill from "../models/skillModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all skills
// @route   GET /api/skills
// @access  Public
const getSkills = asyncHandler(async (req, res) => {
  const skills = await Skill.find();
  res.json({
    status: "success",
    results: skills.length,
    data: { skills },
  });
});

// @desc    Create a skill
// @route   POST /api/skills
// @access  Private/User
const createSkill = asyncHandler(async (req, res) => {
  let newskill = {
    ...req.body,
  };

  const createdskill = await Skill.create(newskill);

  res.status(201).json({
    status: "success",
    data: { createdskill },
  });
});

// @desc    Delete a skill
// @route   DELETE /api/skills/:id
// @access  Private/User
const deleteSkill = asyncHandler(async (req, res) => {
  const deletedSkill = await Skill.findByIdAndDelete(req.params.id);

  if (deletedSkill) {
    res.json({ message: "skill removed" });
  } else {
    throw new AppError("Skill not found", 404);
  }
});

// @desc    Update a skill
// @route   PUT /api/skills/:id
// @access  Private/User
const updateSkill = asyncHandler(async (req, res) => {
  console.log(req.body);
  let toUpdateSkill = {
    ...req.body,
  };

  const updatedSkill = await Skill.findOneAndUpdate(
    { _id: req.params.id },
    { $set: toUpdateSkill },
    { new: true, runValidators: true }
  );

  if (updatedSkill) {
    res.json(updatedSkill);
  } else {
    throw new AppError("Skill not found", 404);
  }
});

export { getSkills, deleteSkill, createSkill, updateSkill };
