import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Recomm from "../models/recommendationModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all recommendations
// @route   GET /api/recommendations
// @access  Public
const getRecommendations = asyncHandler(async (req, res) => {
  const recommendations = await Recomm.find();
  res.json({
    status: "success",
    results: recommendations.length,
    data: { recommendations },
  });
});

// @desc    Create recommendation
// @route   POST /api/recommendations
// @access  Public
const createRecommendation = asyncHandler(async (req, res) => {
  const recommendation = {
    name: req.body.name,
    profession: req.body.profession,
    profile: req.body.profile,
    description: req.body.description,
  };
  if (req.body.company) recommendation["company"] = req.body.company;
  if (req.body.pinned) recommendation["pinned"] = req.body.pinned;
  const createdRecommendation = await Recomm.create(recommendation);

  res.status(201).json({
    status: "success",
    data: { createdRecommendation },
  });
});

export { getRecommendations, createRecommendation };
