import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Education from "../models/educationModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all education
// @route   GET /api/education
// @access  Public
const getEducation = asyncHandler(async (req, res) => {
  const education = await Education.find().sort({ _id: 1 });
  res.json({
    status: "success",
    results: education.length,
    data: { education },
  });
});

export { getEducation };
