import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Faq from "../models/faqModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all faq
// @route   GET /api/faq
// @access  Public
const getFaq = asyncHandler(async (req, res) => {
  const faq = await Faq.find().sort({ _id: 1 });
  res.json({
    status: "success",
    results: faq.length,
    data: { faq },
  });
});

export { getFaq };
