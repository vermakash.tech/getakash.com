import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Initial from "../models/initialModel.js";
import { ProjectRequest } from "../models/initialModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all taglines
// @route   GET /api/initials/taglines
// @access  Public
const getTaglines = asyncHandler(async (req, res) => {
  const taglines = await Initial.find({ type: "TAGLINE" });
  res.json({
    status: "success",
    results: taglines.length,
    data: { taglines },
  });
});

// @desc    Fetch all descriptions
// @route   GET /api/initials/descriptions
// @access  Public
const getDescriptions = asyncHandler(async (req, res) => {
  const descriptions = await Initial.find({ type: "DESCRIPTION" });
  res.json({
    status: "success",
    results: descriptions.length,
    data: { descriptions },
  });
});

// @desc    Create Project Request
// @route   GET /api/initials/projectrequest
// @access  Public
const createProjectRequest = asyncHandler(async (req, res) => {
  console.log(req.body);
  const projectReq = req.body;
  const createdProjectReq = await ProjectRequest.create(projectReq);

  res.status(201).json({
    status: "success",
    data: { createdProjectReq },
  });
});

export { getTaglines, getDescriptions, createProjectRequest };
