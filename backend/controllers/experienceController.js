import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Experience from "../models/experienceModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all experiences
// @route   GET /api/experiences
// @access  Public
const getExperience = asyncHandler(async (req, res) => {
  const experiences = await Experience.find().sort({ _id: 1 });
  res.json({
    status: "success",
    results: experiences.length,
    data: { experiences },
  });
});

export { getExperience };
