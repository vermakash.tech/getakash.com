import mongoose from "mongoose";
import asyncHandler from "express-async-handler";
import Service from "../models/serviceModel.js";
import AppError from "../utils/AppError.js";

// @desc    Fetch all services
// @route   GET /api/services
// @access  Public
const getServices = asyncHandler(async (req, res) => {
  const services = await Service.find();
  res.json({
    status: "success",
    results: services.length,
    data: { services },
  });
});

export { getServices };
