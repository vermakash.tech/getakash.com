const services = [
  {
    title: "Web Designing",
    description:
      "Web designing is a creative field. I design beautiful mobile and web applications. As a web designer, I enjoy using Figma and Adobe ecosystem. Whether you need just a home page design or a creative website design, I'll do that for you.",
    image: "/images/service_webdes.jpg",
    pinned: true,
  },
  {
    title: "Web Development",
    description:
      "I am a frontend and a backend developer conjointly. I have been developing responsive websites since 2019. Web development is a wide niche, I code as a MERN stack developer but also have experience with Mysql, Pug, Next, etc.",
    image: "/images/service_webdev.jpg",
    pinned: true,
  },
  {
    title: "SEO & Hosting",
    description:
      "I help businesses with web hosting and SEO optimization. I have used Heroku, GCP, and other Web hosting providers for website hosting. My website is running on AWS. For boosting the site's SEO, I perform Keyword research, on-page, and off-page optimization.",
    image: "/images/service_seo.jpg",
    pinned: true,
  },
  {
    title: "The Complete Solution",
    description:
      "I am a web developer and designer who knows Hosting and SEO optimization, which makes me a Full stack developer. I love creating websites from scratch, from Design to SEO to Development to Hosting. I'll be very glad to do that for you.",
    image: "/images/service_compsol.jpg",
    pinned: false,
  },
  {
    title: "Wordpress Development",
    description:
      "WordPress is a fast way to get a website for small businesses. You can consider me as a WordPress designer or developer who can create a CMS website with WordPress for you.",
    image: "/images/service_wordpress.jpg",
    pinned: false,
  },
  {
    title: "Ongoing Project Management",
    description:
      "Need just an upgrade for your existing site? You still came to the right place. I can provide website redesign, website maintenance, and ongoing project management. I will take the project from the middle to perfection. Feel free to make a project request.",
    image: "/images/service_opm.jpg",
    pinned: false,
  },
];

export default services