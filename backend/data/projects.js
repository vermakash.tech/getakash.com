const projects = [
  {
    title: "Chefkart",
    description:
      "It is a full ecommerce website with seperate admin access, integrated with paypal for payment. Admin can perform crud operations on Users, Products, Bookings. Made with MERN stack and hosted on Heroku",
    urlLive: "https://chef-kart.herokuapp.com/",
    urlCode: "https://gitlab.com/vermakash.tech/chefkart",
    tech: ["React", "Redux", "Express", "Json Web Token", "Mongoose"],
    tag: "Dev",
    image: "/images/project_chefkart.jpg",
    pinned: true,
  },
  {
    title: "Camping.com",
    description:
      "It is a full stack application, integrated with cloudinary for image upload and Passport for authentication. You can view camps on map, add comments on existing camps and create new camps directly to the database ",
    urlLive: "https://camping-com.herokuapp.com/",
    urlCode: "https://gitlab.com/vermakash.tech/camping.com",
    tech: ["Ejs", "Cloudinary", "NodeJS", "Passport", "Mongoose"],
    tag: "Dev",
    image: "/images/project_camping.jpg",
    pinned: true,
  },
  {
    title: "Covisafe",
    description:
      "It is a React base application, integrated with one of the Rapid APIs for Covid data. You can track current global statistics of Covid. Filtering is implemented for country and for previous dates",
    urlLive: "https://covisafe.herokuapp.com/",
    urlCode: "https://gitlab.com/vermakash.tech/covisafe",
    tech: ["React", "Axios", "Express", "Rapid Api", "Context"],
    tag: "Dev",
    image: "/images/project_covisafe.jpg",
    pinned: true,
  },
  {
    title: "Sde Portfolio",
    description:
      "It is a portfolio website for software developer. Created with plain HTML, CSS, and Javascript. Nodemailer is used for sending and recieving emails through the website. Server is made with Express.",
    urlLive: "https://sde-portfolio.herokuapp.com/",
    urlCode: "https://gitlab.com/vermakash.tech/sde-portfolio",
    tech: ["NodeJS", "Sass", "Express", "Mongoose", "NodeMailer"],
    tag: "Dev",
    image: "/images/project_portfolio.jpg",
    pinned: true,
  },
  {
    title: "Pickolor",
    description:
      "It is a frontend project totally made with React and hosted on heroku. You can copy any color code, make new palettes with the colors of your choice. Context Api is used for state management",
    urlLive: "https://pickolor-getakash.netlify.app/",
    urlCode: "https://gitlab.com/vermakash.tech/pickolor",
    tech: ["React", "Context", "Chroma", "Material-UI", "Netlify"],
    tag: "Dev",
    image: "/images/project_pickolor.jpg",
    pinned: true,
  },
  {
    title: "Trillo",
    description:
      "It is a template page, fully responsive and made with HTML, SASS. CSS Flexbox is used majorly for the layout. It was my first design project 😄",
    urlLive: "https://trillo-getakash.netlify.app/",
    urlCode: "https://gitlab.com/vermakash.tech/Trillo-Design",
    tech: ["HTML", "Sass", "Flexbox", "Netlify", "Bootstrap"],
    tag: "Des",
    image: "/images/project_trillo.jpg",
    pinned: false,
  },
  {
    title: "Nexter",
    description:
      "It is a template home page of a Real Estate website, fully responsive. CSS grids are used majorly for the layout and Netlify for the hosting.",
    urlLive: "https://nexter-getakash.netlify.app/",
    urlCode: "https://gitlab.com/vermakash.tech/nexter",
    tech: ["HTML", "Sass", "Grids", "Netlify", "Bootstrap"],
    tag: "Des",
    image: "/images/project_nexter.jpg",
    pinned: false,
  },
  {
    title: "Perfect Gym",
    description:
      "It is a Figma project. A web design for the perfect fitness gym. Pages includes Homepage, Classes, Pricing, Login and an About page.",
    urlLive:
      "https://www.figma.com/proto/uLZgYMI3OerZi6R0d9guf5/Perfect-Fitness?node-id=67%3A227&scaling=scale-down&page-id=67%3A226&starting-point-node-id=67%3A227",
    tech: ["Figma", "Made with ❤️"],
    tag: "Des",
    image: "/images/project_fitness.jpg",
    pinned: false,
  },
  {
    title: "Getakash.com",
    description:
      "It is the web design of the very website you are visiting now. Fully designed on figma from Site-layout to Wireframe to the final Prototype.",
    urlLive:
      "https://www.figma.com/proto/rCQBy5DzNUaytMDzxczxdP/Portfolio?node-id=77%3A352&scaling=contain&page-id=76%3A351&starting-point-node-id=77%3A352",
    tech: ["Figma", "Made with ❤️"],
    tag: "Des",
    image: "/images/project_getakash.jpg",
    pinned: false,
  },
  {
    title: "Travel App",
    description:
      "It is a simple web design for the mobile application of a travel and flight booking app. Also designed on Figma from wireframe to the Prototype.",
    urlLive:
      "https://www.figma.com/proto/ziQ2brPligvTHYNss3RHhU/Travel-App?node-id=2%3A4&scaling=scale-down&page-id=0%3A1",
    tech: ["Figma", "Made with ❤️"],
    tag: "Des",
    image: "/images/project_travel.jpg",
    pinned: false,
  },
];

export default projects