const certifications = [
    {
        title: "Practical Web Designing",
        image: "/images/certi_webdes.jpg"
    },
    {
        title: "Web Developer Bootcamp",
        image: "/images/certi_webdev.jpg"
    },
    {
        title: "MySql for data analytics",
        image: "/images/certi_mysql.jpg"
    },
    {
        title: "The Advanced Javascript",
        image: "/images/certi_js.jpg"
    },
    {
        title: "Algorithm Toolbox",
        image: "/images/certi_algo.jpg"
    },
    {
        title: "AWS Fundamentals",
        image: "/images/certi_aws.jpg"
    },
    {
        title: "SEO Training Masterclass",
        image: "/images/certi_seo.jpg"
    },
    {
        title: "Advanced CSS Concepts",
        image: "/images/certi_css.jpg"
    },
    {
        title: "Modern React Bootcamp",
        image: "/images/certi_react.jpg"
    },
]

export default certifications