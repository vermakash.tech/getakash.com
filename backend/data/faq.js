const faq = [
  {
    q: "How much time does it take to complete a project?",
    a: "It totally depends on the Project. It might take 1 week to even 2 months",
  },
  {
    q: "How much do you charge for a single project?",
    a: "More straightforward projects can cost from 100 USD (5000 INR), and more complex ones might cost above 1000 USD (50000 INR)",
  },
  {
    q: "What services do you provide?",
    a: "Basically anything related to web, whether it's frontend, backend, web design, seo managemet, hosting, etc",
  },
  {
    q: "Why do I need a website?",
    a: "Having a good website instantly boosts your credibility as a legitimate business. First impressions count, and websites allow you to make a very strong impression with a well designed introduction for your business",
  },
  {
    q: "What type of websites do you build?",
    a: "I build websites to help individuals and businesses to boost their online presence. Ecommerce, Fashion, Real Estate, Photography, Saas, and Blogs are some niches of it",
  },
  {
    q: "What languages do you speak?",
    a: "I am fluent in English and Hindi",
  },
  {
    q: "What type of businesses do you work with?",
    a: "I am open to work with any type of business, whether it's related to Fitness, Saas, Delivery, Education or any other that I can't think of right now",
  },
  {
    q: "Do you provide social media marketing or consulting services?",
    a: "No, not at this point in time",
  },
];

export default faq