const descriptions = [
  {
    page: "home",
    primary:
      "I am a Software Developer by profession. I make wonderful creative websites. My mission is to build a solid web presence for you and your business. You can consider me as a website developer, website creator or a full stack developer which means that I can help you with pretty much everything related to web.",
  },
  {
    page: "services",
    primary: "Who am I?",
    secondary:
      "I’m the complete package, whether you need a Designer, a Front end developer, a Backend developer, or a person who can manage SEO and hostings, I am the full stack developer for you.",
  },
  {
    page: "blogs",
    primary:
      "I am a big fan of Agile methodologies and we all share the same thirst for learning software development. I regularly post about latest technologies. Scroll down to read latest blogs",
  },
  {
    page: "about",
    primary:
      "<span>Hi!</span> This is Akash Verma. I am a <span>Software Developer </span> by profession. I have studied Mechanical Engineering from <span>IIT (BHU) Varanasi</span>. I'm passionate about learning the latest and greatest software technologies. I enjoy creating sleek and responsive applications in addition to being user friendly. I love working in fast paced environments. With my cultural and sports experiences I excel at collaborating with teams of the same or different knowledge sets.",
    secondary:
      "A versatile <span>Engineer</span> with interests in developing software applications. A sportsman and a <span>Boxer</span>. Adaptable and self motivated learner. Interested in new programming technologies, and continuous self improvement.",
  },
];

export default descriptions