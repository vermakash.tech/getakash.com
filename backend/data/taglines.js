const taglines = [
  {
    page: "home",
    primary: "Welcome to my web dev Portfolio , let’s create superb things",
  },
  {
    page: "services",
    primary: "Web presence on google is not difficult in 2022.",
    secondary: "As a web developer, I can help you the right way.",
  },
  {
    page: "blogs",
    primary: "What’s the object-oriented way to get wealthy? Inheritance.",
    secondary: "Programmer: A machine that turns coffee into code.",
    tertiary: 'JavaScript logic: 0 == "0" and 0 == []; therefore, "0" != []',
  },
  {
    page: "about",
    primary: "NA",
  },
];

export default taglines