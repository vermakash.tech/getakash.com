const education = [
  {
    college: "Indian Institute of Technology (BHU) Varanasi",
    course: "B.Tech",
    profile: "https://www.linkedin.com/school/iitbhu-varanasi/",
    specialization: "Mechanical Engineering",
    from: 2018,
    to: 2022,
    gradeType: "CGPA",
    grade: 8.96,
    logo: "/images/logo_iitbhu.jpg",
  },
  {
    college: "Dr. C.V.Raman Intermediate College",
    course: "Intermediate",
    specialization: "P C M",
    from: 2015,
    to: 2017,
    gradeType: "Percentage",
    grade: 89.4,
    logo: "/images/logo_cvric.jpg",
  },
  {
    college: "Zero to Mastery Academy",
    profile: "https://www.linkedin.com/school/ztm-academy/",
    course: "Software Development",
    from: 2020,
    to: 2024,
    logo: "/images/logo_ztm.jpg",
  },
];

export default education