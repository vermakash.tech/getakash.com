const experiences = [
  {
    title: "Freelance Designer & Developer",
    type: "Freelance",
    company: "Getakash.com",
    profile: "https://getakash.com",
    startDate: "2022-07-31T18:30:00.000Z",
    endDate: "2025-05-19T18:30:00.000Z",
    durationType: "Months",
    duration: 4,
    description: [
      "Created various full-stack websites with nodejs, mongodb, react, next, and other cool technologies",
      "Designed landing pages and full websites of various niches",
      "Implemented SEO on various websites to increase their ranks. This website is a great example.",
      "Helped various businesses to boost their online presence.",
    ],
    logo: "/images/logo_getakash.svg",
  },
  {
    title: "Backend Developer",
    type: "Internship",
    company: "Airblack",
    profile: "https://www.linkedin.com/company/airblack/",
    startDate: "2022-01-31T18:30:00.000Z",
    endDate: "2022-05-19T18:30:00.000Z",
    durationType: "Months",
    duration: 4,
    description: [
      "Implemented extra credits feature for users to join premium workshops.",
      "Automated exam functionality and comms messages on app.",
      "Created and updated various cron jobs for sending vouchers, updating batches, releasing certificates, etc.",
      "Developed various apps and pages on Retool.",
    ],
    logo: "/images/logo_airblack.jpg",
  },
  {
    title: "Junior Coach",
    type: "Internship",
    company: "Unschool",
    profile: "https://www.linkedin.com/company/unschool-it/",
    startDate: "2021-06-31T18:30:00.000Z",
    endDate: "2021-09-29T18:30:00.000Z",
    durationType: "Months",
    duration: 3,
    description: [
      "Taught over 1000 students about Full-Stack Development.",
      "Improved my interaction skills by giving live sessions.",
      "Technologies Taught - HTML, CSS, React, Bootstrap, MySql, etc.",
    ],
    logo: "/images/logo_unschool.jpg",
  },
];

export default experiences;
