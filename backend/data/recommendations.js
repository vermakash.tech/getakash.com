const recommendations = [
    {
        name: 'Tony Stark',
        profession: 'Super Hero',
        profile: 'https://www.marvel.com/characters/iron-man-tony-stark',
        company: 'Marvel',
        pinned: true,
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution'
    },
    {
        name: 'Wanda Maximoff',
        profession: 'Super Hero',
        profile: 'https://www.marvel.com/characters/scarlet-witch-wanda-maximoff',
        company: 'Marvel',
        pinned: true,
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution'
    },
    {
        name: 'Rajesh Koothrappali',
        profession: 'Astrophysicist (scientist)',
        profile: 'https://bigbangtheory.fandom.com/wiki/Rajesh_Koothrappali',
        company: 'Big Bang Theory',
        pinned: true,
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution'
    },
    {
        name: 'Wanda Maximoff',
        profession: 'Super Hero',
        profile: 'https://www.marvel.com/characters/scarlet-witch-wanda-maximoff',
        company: 'Marvel',
        pinned: true,
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution'
    },
]

export default recommendations