const skills = [
    {
        name: "NodeJS",
        logo: "/images/logo_nodejs.svg",
        pinned: true
    },
    {
        name: "React",
        logo: "/images/logo_react.svg",
        pinned: true
    },
    {
        name: "Wordpress",
        logo: "/images/logo_wordpress.svg",
        pinned: true
    },
    {
        name: "Figma",
        logo: "/images/logo_figma.svg",
        pinned: true
    },
    {
        name: "Github",
        logo: "/images/logo_github.svg",
        pinned: true
    },
    {
        name: "MySql",
        logo: "/images/logo_mysql.svg",
        pinned: true
    },
    {
        name: "MongoDB",
        logo: "/images/logo_mongodb.svg",
        pinned: true
    },
    {
        name: "NextJS",
        logo: "/images/logo_next.svg",
        pinned: true
    },
    {
        name: "AWS",
        logo: "/images/logo_aws.svg",
        pinned: false
    },
    {
        name: "C",
        logo: "/images/logo_c.svg",
        pinned: false
    },
    {
        name: "C++",
        logo: "/images/logo_cpp.svg",
        pinned: false
    },
    {
        name: "Python",
        logo: "/images/logo_python.svg",
        pinned: false
    },
    {
        name: "JWT",
        logo: "/images/logo_jwt.svg",
        pinned: false
    },
    {
        name: "Sass",
        logo: "/images/logo_sass.svg",
        pinned: false
    },
    {
        name: "Retool",
        logo: "/images/logo_retool.svg",
        pinned: false
    },
]

export default skills