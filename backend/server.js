import path from 'path'
import express from 'express'
import dotenv from 'dotenv'
import morgan from 'morgan'
import colors from 'colors'
import cors from "cors";
import connectDB from "./configs/db.js";
import skillRoutes from "./routes/skillRoutes.js";
import serviceRoutes from "./routes/serviceRoutes.js";
import recommendationRoutes from "./routes/recommendationRoutes.js";
import projectRoutes from "./routes/projectRoutes.js";
import initialRoutes from "./routes/initialRoutes.js";
import faqRoutes from "./routes/faqRoutes.js";
import experienceRoutes from "./routes/experienceRoutes.js";
import educationRoutes from "./routes/educationRoutes.js";
import certificationRoutes from "./routes/certificationRoutes.js";
import globalErrorHandler from "./middlewares/errorMiddleware.js";
import AppError from "./utils/AppError.js";

dotenv.config();
connectDB();
const app = express();

//Uncaught Exceptions
process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT EXCEPTION! 💥 Shutting down...".red.bold);
  console.log(err.name.brightRed, err.message.brightRed);
  process.exit(1);
});

//pre request middlewares
app.use(cors());
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(express.json());

//requests
app.use("/api/skills", skillRoutes);
app.use("/api/services", serviceRoutes);
app.use("/api/recommendations", recommendationRoutes);
app.use("/api/projects", projectRoutes);
app.use("/api/initials", initialRoutes);
app.use("/api/faq", faqRoutes);
app.use("/api/experiences", experienceRoutes);
app.use("/api/education", educationRoutes);
app.use("/api/certifications", certificationRoutes);

//for invalid end points
app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});

//post request middlewares
app.use(globalErrorHandler);

//server connection
const PORT = process.env.SERVER_PORT || 5000;
const server = app.listen(PORT, () => {
  console.log(`############### Server running on port ${PORT} ##############`.magenta.bold);
});

//Unhandled rejections
process.on('unhandledRejection', err => {
  console.log('UNHANDLED REJECTION! 💥 Shutting down...'.red.bold);
  console.log(err.name.brightRed, err.message.brightRed);
  server.close(() => {
    process.exit(1);
  });
});