import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const experienceSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  company: {
    type: String,
    required: true,
  },
  profile: {
    type: String,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
  durationType: {
    type: String,
    required: true,
  },
  duration: {
    type: Number,
    required: true,
  },
  description: [],
  logo: {
    type: String,
    required: true,
  },
  rank:{
    type: Number,
    required: true,
  },
});

const Experience = mongoose.model("experience", experienceSchema);

export default Experience;
