import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const projectSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  urlLive: {
    type: String,
  },
  urlCode: {
    type: String,
  },
  tech: [],
  tag: {
    type: String,
    enum: ["Dev", "Des"],
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  pinned: {
    type: Boolean,
    default: false,
  },
  rank:{
    type: Number,
  },
});

const Project = mongoose.model("project", projectSchema);

export default Project;
