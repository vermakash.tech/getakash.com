import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const skillSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  logo: {
    type: String,
    required: true,
  },
  pinned: {
    type: Boolean,
    default: false,
  },
});

const Skill = mongoose.model("skill", skillSchema);

export default Skill;
