import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const recommSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  profession: {
    type: String,
    required: true,
  },
  profile: {
    type: String,
    required: true,
  },
  company: {
    type: String,
  },
  description: {
    type: String,
    required: true,
  },
  pinned: {
    type: Boolean,
    default: false,
  },
});

const Recomm = mongoose.model("recomm", recommSchema);

export default Recomm;
