import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const certiSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});

const Certi = mongoose.model("certi", certiSchema);

export default Certi;
