import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const educationSchema = mongoose.Schema({
  college: {
    type: String,
    required: true,
  },
  course: {
    type: String,
    required: true,
  },
  profile: {
    type: String,
  },
  specialization: {
    type: String,
  },
  from: {
    type: Number,
    required: true,
  },
  to: {
    type: Number,
    required: true,
  },
  gradeType: {
    type: String,
  },
  grade: {
    type: Number,
  },
  logo: {
    type: String,
    required: true,
  },
});

const Education = mongoose.model("education", educationSchema);

export default Education;
