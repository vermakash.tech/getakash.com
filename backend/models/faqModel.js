import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const faqSchema = mongoose.Schema({
  q: {
    type: String,
    required: true,
  },
  a: {
    type: String,
    required: true,
  },
});

const Faq = mongoose.model("faq", faqSchema);

export default Faq;
