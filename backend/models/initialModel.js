import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const initialSchema = mongoose.Schema({
  type: {
    type: String,
    enum: ["TAGLINE", "DESCRIPTION"],
    required: true,
  },
  page: {
    type: String,
    required: true,
  },
  primary: {
    type: String,
    required: true,
  },
  secondary: {
    type: String,
  },
  tertiary: {
    type: String,
  },
});

const projectRequestSchema = mongoose.Schema({
  projectType: {
    type: String,
  },
  websiteType: {
    type: String,
  },
  techStack: {
    type: String,
  },
  seoService: {
    type: String,
  },
  hostingService: {
    type: String,
  },
  timeLimit: {
    type: String,
  },
  email: {
    type: String,
  },
  name: {
    type: String,
  },
  contact: {
    type: String,
  },
  language: {
    type: String,
  },
  additionalInfo: {
    type: String,
  },
});

const ProjectRequest = mongoose.model("projectRequest", projectRequestSchema);
const Initial = mongoose.model("initial", initialSchema);

export { ProjectRequest };
export default Initial;

//   title: {
//     type: String,
//     required: true,
//   },
//   tag: {
//     type: String,
//     enum: ["Productivity", "General", "Study", "Fitness", "Chores"],
//   },
//   // author: [authorSchema],
//   createdAt: {
//     type: Date,
//     default: Date.now(),
//     required: true,
//   },
//   isCompleted: {
//     type: Boolean,
//     required: true,
//     default: false,
//   },
