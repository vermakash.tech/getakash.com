import mongoose from "mongoose";
const ObjectId = mongoose.Schema.Types.ObjectId;

const serviceSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  pinned: {
    type: Boolean,
    default: false,
  },
});

const Service = mongoose.model("service", serviceSchema);

export default Service;
