import mongoose from 'mongoose'
import dotenv from 'dotenv'
import colors from 'colors'
import taglines from "./data/taglines.js";
import descriptions from "./data/descriptions.js";
import skills from "./data/skills.js";
import connectDB from "./configs/db.js";
import Initial from "./models/initialModel.js";
import Skill from "./models/skillModel.js";
import Service from "./models/serviceModel.js";
import services from "./data/services.js";
import Recomm from "./models/recommendationModel.js";
import recommendations from "./data/recommendations.js";
import Project from "./models/projectModel.js";
import projects from "./data/projects.js";
import Faq from "./models/faqModel.js";
import faq from "./data/faq.js";
import Experience from "./models/experienceModel.js";
import experiences from "./data/experiences.js";
import Education from "./models/educationModel.js";
import education from "./data/education.js";
import certifications from "./data/certifications.js";
import Certi from "./models/certificationModel.js";

dotenv.config();
connectDB();

const importData = async () => {
  try {
    //deletions
    await Initial.deleteMany();
    await Skill.deleteMany();
    await Service.deleteMany();
    await Recomm.deleteMany();
    await Project.deleteMany();
    await Faq.deleteMany();
    await Experience.deleteMany();
    await Education.deleteMany();
    await Certi.deleteMany();

    //data build up
    const descriptionData = descriptions.map((x) => {
      return { ...x, type: "DESCRIPTION" };
    });
    const taglineData = taglines.map((x) => {
      return { ...x, type: "TAGLINE" };
    });
    const skillData = skills;
    const serviceData = services;
    const recommData = recommendations;
    const projectData = projects;
    const faqData = faq;
    const experienceData = experiences;
    const educationData = education;
    const certiData = certifications;

    //creations
    await Initial.insertMany(taglineData);
    await Initial.insertMany(descriptionData);
    await Skill.insertMany(skillData);
    await Service.insertMany(serviceData);
    await Recomm.insertMany(recommData);
    await Project.insertMany(projectData);
    await Faq.insertMany(faqData);
    await Experience.insertMany(experienceData);
    await Education.insertMany(educationData);
    await Certi.insertMany(certiData);

    console.log("Data Imported!".green.inverse);
    process.exit();
  } catch (error) {
    console.error(`${error}`.red.inverse);
    process.exit(1);
  }
};

const destroyData = async () => {
  try {
    await Task.deleteMany()
    await User.deleteMany()

    console.log('Data Destroyed!'.red.inverse)
    process.exit()
  } catch (error) {
    console.error(`${error}`.red.inverse)
    process.exit(1)
  }
}

if (process.argv[2] === '-d') {
  destroyData()
} else {
  importData()
}
