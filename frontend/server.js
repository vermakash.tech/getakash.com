// USING CUSTOM-ROUTE SLOWS THE PAGE NAVIGATION (USE IT ONLY WHEN REALLY NECESSARY)

// const { createServer } = require('http')
// const { parse } = require('url')
const express = require('express')
const next = require('next')

const dev = process.env.NODE_ENV !== 'production'
const hostname = 'localhost'
const port = 3000
// when using middleware `hostname` and `port` must be provided below
const app = next({ dev, hostname, port })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()

  server.get('/custom-route', (req,res)=>{
    app.render(req, res, '/dummy', {})
  })

//   server.get('/custom-route/:id', (req,res)=>{
//     let id = req.params.id
//     app.render(req, res, '/dummy-page', {id}) //last arguement is for the query props (accesible in dummy-page.js)
//   })

  server.get('*', (req,res)=>{
    return handle(req, res)
  })

  server.listen(process.env.PORT || 3000, (err)=>{
    if(err) throw err;
    console.log(`##### Serving on port ${process.env.PORT ? process.env.PORT  : 3000} #####`)
  })
})