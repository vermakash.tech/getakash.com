import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import StyledAdmin from "../styles/pages/Admin.styled";
import Footer from "../components/Footer";
import Popup from "../components/Popup";
import ProjectRequest from "../components/ProjectRequest";
import { useDispatch, useSelector } from "react-redux";
import { closePopup, openPopup, setPopupData } from "../redux/slices/popupSlice";
import axios from "axios";

export default function admin() {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [profession, setProfession] = useState("");
  const [profile, setProfile] = useState("");
  const [description, setDescription] = useState("");
  const isProjectFlow = useSelector((state) => state.projectFlow.data.isProjectFlow);
  const { isPopupOpen, popupMessage, popupType } = useSelector((state) => state.popup.data);

  useEffect(() => {}, []);
  const handleSubmit = async (event) => {
    event.preventDefault();
    //Logic here
    let body = {
      name,
      profession,
      profile,
      description,
    };
    try {
      const recommRes = await axios.post("http://127.0.0.1:5000/api/recommendations", body);
      if (recommRes) {
        dispatch(setPopupData({ msg: "Recommendation recieved successfully", type: "THANK YOU" }));
        dispatch(openPopup());
        setName("");
        setProfession("");
        setProfile("");
        setDescription("");
      }
    } catch (error) {
      console.log(error);
      alert("Something went wrong");
    }
  };
  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleProfessionChange = (event) => {
    setProfession(event.target.value);
  };
  const handleProfileChange = (event) => {
    setProfile(event.target.value);
  };
  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };
  return (
    <StyledAdmin>
      <div className="admin_container">
        <div className="admin_login">
          <form onSubmit={(e) => handleSubmit(e)}>
            <input value={name} placeholder="Your Name" onChange={(e) => handleNameChange(e)} required></input>
            <input
              value={profession}
              placeholder="Your Profession"
              onChange={(e) => handleProfessionChange(e)}
              required
            ></input>
            <input value={profile} placeholder="Profile Link" onChange={(e) => handleProfileChange(e)} required></input>
            <textarea
              value={description}
              placeholder="Description"
              onChange={(e) => handleDescriptionChange(e)}
              required
            ></textarea>
            <button type="submit">Submit</button>
          </form>
        </div>
        <div className="admin_screen"></div>
      </div>
      {isProjectFlow ? <ProjectRequest /> : null}
      {isPopupOpen ? <Popup message={popupMessage} type={popupType} /> : null}
    </StyledAdmin>
  );
}
