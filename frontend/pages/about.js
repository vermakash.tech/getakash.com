import { useEffect, useState } from "react";
import { wrapper } from "../redux/store";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";
import { fetchInitials } from "../redux/actions/initialActions";
import { fetchData } from "../redux/actions/aboutMeActions";
import { fetchSkills } from "../redux/actions/skillActions";
import Footer from "../components/Footer";
import Skills from "../components/Skills";
import ProjectRequest from "../components/ProjectRequest";
import StyledAbout from "../styles/pages/About.styled";
import LandingAbout from "../components/LandingAbout";
import ResumeAbout from "../components/ResumeAbout";
import Popup from "../components/Popup";

export default function Services(props) {
  const isProjectFlow = useSelector((state) => state.projectFlow.data.isProjectFlow);
  const { isPopupOpen, popupMessage, popupType } = useSelector((state) => state.popup.data);

  return (
    <StyledAbout>
      <LandingAbout />
      <Skills fillBackground={true} />
      <ResumeAbout />
      <Footer />
      {isProjectFlow ? <ProjectRequest /> : null}
      {isPopupOpen ? <Popup message={popupMessage} type={popupType} /> : null}
    </StyledAbout>
  );
}

export const getStaticProps = wrapper.getStaticProps((store) => async ({ req, res }) => {
  const inits = await store.dispatch(fetchInitials({ page: "about" }));
  const data = await store.dispatch(fetchData());
  const skills = await store.dispatch(fetchSkills());
  //add more dispatches here
  return {
    props: {},
    revalidate: 10,
  };
});
