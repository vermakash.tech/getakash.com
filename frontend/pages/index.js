import { useEffect, useState } from "react";
import {wrapper} from '../redux/store'
import { useSelector, useDispatch } from 'react-redux'
import Link from 'next/link'
import 'bootstrap/dist/css/bootstrap.min.css'
import StyledIndex from "../styles/pages/Index.styled";
import Landing from "../components/Landing";
import { fetchInitials } from "../redux/actions/initialActions";
import { fetchProjects } from "../redux/actions/projectActions";
import { fetchRecommendations } from "../redux/actions/recommendationActions";
import { fetchServices } from "../redux/actions/serviceAction";
import { fetchSkills } from "../redux/actions/skillActions";
import Skills from "../components/Skills";
import ServicesHome from "../components/ServicesHome";
import ProjectsHome from "../components/ProjectsHome";
import Footer from "../components/Footer";
import Recommendations from "../components/Recommendations";
import ProjectRequest from "../components/ProjectRequest";
import Popup from "../components/Popup";

export default function Home(props) {
  const isProjectFlow = useSelector((state) => state.projectFlow.data.isProjectFlow);
  const { isPopupOpen, popupMessage, popupType } = useSelector((state) => state.popup.data);

  return (
    <StyledIndex>
      <Landing />
      <Skills />
      <ServicesHome />
      <ProjectsHome />
      <Recommendations />
      <Footer />
      {isProjectFlow ? <ProjectRequest /> : null}
      {isPopupOpen ? <Popup message={popupMessage} type={popupType} /> : null}
    </StyledIndex>
  );
}

export const getStaticProps = wrapper.getStaticProps((store) => async ({ req, res }) => {
  const inits = await store.dispatch(fetchInitials({ page: "home" }));
  const projs = await store.dispatch(fetchProjects());
  const reccs = await store.dispatch(fetchRecommendations());
  const servs = await store.dispatch(fetchServices());
  const skills = await store.dispatch(fetchSkills());
  //add more dispatches here
  return {
    props: {},
    revalidate: 60,
  };
});
