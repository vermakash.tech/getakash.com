import { useEffect, useState } from "react";
import { wrapper } from "../redux/store";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";
import StyledServices from "../styles/pages/Services.styled";
import { fetchInitials } from "../redux/actions/initialActions";
import { fetchServices } from "../redux/actions/serviceAction";
import { fetchFaq } from "../redux/actions/faqActions";
import LandingServices from "../components/LandingServices";
import ServicesServices from "../components/ServicesServices";
import Footer from "../components/Footer";
import Faq from "../components/Faq";
import Workflow from "../components/Workflow";
import ProjectRequest from "../components/ProjectRequest";
import Popup from "../components/Popup";

export default function Services(props) {
  const isProjectFlow = useSelector((state) => state.projectFlow.data.isProjectFlow);
  const { isPopupOpen, popupMessage, popupType } = useSelector((state) => state.popup.data);

  return (
    <StyledServices>
      <LandingServices />
      <ServicesServices />
      <Faq />
      <Workflow />
      <Footer />
      {isProjectFlow ? <ProjectRequest /> : null}
      {isPopupOpen ? <Popup message={popupMessage} type={popupType} /> : null}
    </StyledServices>
  );
}

export const getStaticProps = wrapper.getStaticProps((store) => async ({ req, res }) => {
  const inits = await store.dispatch(fetchInitials({ page: "services" }));
  const servs = await store.dispatch(fetchServices());
  const faqs = await store.dispatch(fetchFaq());
  //add more dispatches here
  return {
    props: {},
    revalidate: 10,
  };
});
