import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import StyledAdmin from "../styles/pages/Admin.styled";
import ProjectRequest from "../components/ProjectRequest";
import { setCookie, getCookie } from "../utilities/cookies";

export default function admin() {
  const [isLoggedin, setIsLoggedin] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [inpUsername, setInpUsername] = useState("");
  const [inpPassword, setInpPassword] = useState("");
  const isProjectFlow = useSelector((state) => state.projectFlow.data.isProjectFlow);

  useEffect(() => {}, []);
  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitted(true);
  };
  const handleUsernameChange = (event) => {
    setInpUsername(event.target.value);
    setIsSubmitted(false);
  };
  const handlePasswordChange = (event) => {
    setInpPassword(event.target.value);
    setIsSubmitted(false);
  };
  return (
    <StyledAdmin>
      <div className="admin_container">
        <div className="admin_login">
          <p id="wrongEmailPwd" className={isSubmitted && !isLoggedin ? "admin_login-shake" : "admin_login-hidden"}>
            Wrong username or password
          </p>
          <form onSubmit={(e) => handleSubmit(e)}>
            <input placeholder="Username" onChange={(e) => handleUsernameChange(e)} required></input>
            <input placeholder="Password" type="password" onChange={(e) => handlePasswordChange(e)} required></input>
            <button type="submit">Submit</button>
          </form>
        </div>
        <div className="admin_screen"></div>
      </div>
      {isProjectFlow ? <ProjectRequest /> : null}
    </StyledAdmin>
  );
}
