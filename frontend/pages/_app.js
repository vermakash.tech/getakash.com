import { Router } from 'next/router';
import React from 'react';
import Script from "next/script";
import Footer from "../components/Footer";
import Loader from "../components/Loader";
import Navbar from "../components/Navbar";
import { wrapper } from "../redux/store";
import GlobalStyle from "../styles/Global.styled";
import UtilityStyle from "../styles/Utilities.styled";

function MyApp({ Component, pageProps }) {
  const [loading, setLoading] = React.useState(false);
  React.useEffect(() => {
    const start = () => {
      console.log("start");
      setLoading(true);
    };
    const end = () => {
      console.log("finished");
      setLoading(false);
    };
    Router.events.on("routeChangeStart", start);
    Router.events.on("routeChangeComplete", end);
    Router.events.on("routeChangeError", end);
    return () => {
      Router.events.off("routeChangeStart", start);
      Router.events.off("routeChangeComplete", end);
      Router.events.off("routeChangeError", end);
    };
  }, [loading]);
  return (
    <>
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-W9V1PZ9XD3" strategy="afterInteractive" />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-W9V1PZ9XD3');
        `}
      </Script>
      <UtilityStyle />
      <GlobalStyle />
      <Navbar />
      {loading ? <Loader /> : <Component {...pageProps} />}
    </>
  );
}

export default wrapper.withRedux(MyApp)
