import { useEffect, useState } from "react";
import { wrapper } from "../redux/store";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";
import { fetchProjects } from "../redux/actions/projectActions";
import StyledProjects from "../styles/pages/Projects.styled";
import ProjectCard from "../components/_ProjectCard";
import Footer from "../components/Footer";
import ProjectRequest from "../components/ProjectRequest";
import Popup from "../components/Popup";

export default function Home(props) {
  const [projectsType, setProjectsType] = useState("Dev");
  const isProjectFlow = useSelector((state) => state.projectFlow.data.isProjectFlow);
  const projects = useSelector((state) => state.projects.data);
  const { isPopupOpen, popupMessage, popupType } = useSelector((state) => state.popup.data);

  let displayProjects;
  if (projectsType == "Dev") {
    displayProjects = projects.filter((x) => x.tag == "Dev");
  } else if (projectsType == "Des") {
    displayProjects = projects.filter((x) => x.tag == "Des");
  }
  const handleFilterProjects = (tag) => {
    setProjectsType(tag);
  };
  return (
    <StyledProjects className="projects_page">
      <div className="my-container">
        <h1 className="projects_page-heading">Professional Projects</h1>
        <div className="projects_page-filter">
          <span
            className={`projects_page-filter-button ${
              projectsType == "Des" ? "projects_page-filter-button-active" : ""
            }`}
            onClick={() => handleFilterProjects("Des")}
          >
            Design
          </span>
          <span
            className={`projects_page-filter-button ${
              projectsType == "Dev" ? "projects_page-filter-button-active" : ""
            }`}
            onClick={() => handleFilterProjects("Dev")}
          >
            Development
          </span>
        </div>
        <div className="row projects_page-row">
          {displayProjects.map((x, i) => (
            <div key={i} className="col-lg-4 col-md-6 col-12 projects_page-column">
              <ProjectCard
                urlLive={x.urlLive}
                urlCode={x.urlCode}
                key={projectsType}
                desc={x.description}
                bg_image={x.image}
                title={x.title}
                isShadow={true}
                mainAnimation={"fade_in"}
              />
            </div>
          ))}
        </div>
      </div>
      <Footer />
      {isProjectFlow ? <ProjectRequest /> : null}
      {isPopupOpen ? <Popup message={popupMessage} type={popupType} /> : null}
    </StyledProjects>
  );
}

export const getStaticProps = wrapper.getStaticProps((store) => async ({ req, res }) => {
  const projs = await store.dispatch(fetchProjects());
  //add more dispatches here
  return {
    props: {},
    revalidate: 10,
  };
});
