import React from "react";
import StyledWorkflow from "../styles/components/Workflow.styled";

export default function Workflow() {
  return (
    <StyledWorkflow>
      <h1>My Ideal Work Flow</h1>
      <img src="/images/workflow.svg" />
    </StyledWorkflow>
  );
}
