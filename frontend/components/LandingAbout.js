import React from "react";
import { useSelector, useDispatch } from "react-redux";
import StyledLandingAbout from "../styles/components/LandingAbout.styled";
import parse from "html-react-parser";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faPhone } from "@fortawesome/free-solid-svg-icons";
import { faLinkedin, faWhatsapp, faInstagram } from "@fortawesome/free-brands-svg-icons";

export default function LandingAbout() {
  const { description } = useSelector((state) => state.initials.data);

  return (
    <StyledLandingAbout>
      <div className="my-container landing_about">
        <div className="row landing_about-row">
          <div className="col-lg-6 landing_about-col">
            <img src="/images/about_img.png" alt="Akash Verma" title="Akash Verma"></img>
          </div>
          <div className="col-lg-6 landing_about-col landing_about-desc">
            <h2 className="landing_about-desc-primary">{parse(description.primary)}</h2>
            <h2 className="landing_about-desc-secondary">{parse(description.secondary)}</h2>
            <div className="landing_about-desc-link_container">
              <a href="mailto:vermakash.tech@gmail.com" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="landing_about-desc-link" icon={faEnvelope} />
              </a>
              <a href="https://www.linkedin.com/in/getakash/" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="landing_about-desc-link" icon={faLinkedin} />
              </a>
              <a href="https://www.instagram.com/vermakash.1.2.9/" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="landing_about-desc-link" icon={faInstagram} />
              </a>
              <a href="https://api.whatsapp.com/send?phone=7054443300" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="landing_about-desc-link" icon={faWhatsapp} />
              </a>
              <a href="tel:+91-7054443300" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="landing_about-desc-link" icon={faPhone} />
              </a>
            </div>
          </div>
        </div>
      </div>
    </StyledLandingAbout>
  );
}
