import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import StyledFaq from "../styles/components/Faq.styled";
import FaqCard from "./_FaqCard";

export default function Faq() {
  const faqs = useSelector((state) => state.faq.data);
  console.log(typeof faqs);
  return (
    <StyledFaq id="faq" className="my-container faq">
      <h1 className="faq-heading">F.A.Q</h1>
      <div className="row faq-row">
        {faqs.map((x, i) => (
          <div key={i} className="col-md-6 col-12 faq-col">
            <FaqCard ques={x.q} ans={x.a} />
          </div>
        ))}
      </div>
    </StyledFaq>
  );
}
