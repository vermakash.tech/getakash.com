import React from "react";
import { useSelector } from "react-redux";
import StyledServicesHome from "../styles/components/ServicesHome.styled";
import ServiceCard from "./_ServiceCard";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";
import SiteLink from "./_SiteLink";

export default function ServicesHome() {
  const services = useSelector((state) => state.services.data);
  const servicesHome = services.filter((x) => x.pinned);

  return (
    <StyledServicesHome>
      <div className="servicesHome">
        <div className="servicesHome-content">
          <div className="my-container">
            <h1 className="servicesHome-heading">
              What can I help you with?&nbsp;
              <Link href="/services">
                <FontAwesomeIcon className="servicesHome-heading-redirect" icon={faArrowUpRightFromSquare} />
              </Link>
            </h1>
            <h2 className="servicesHome-desc">
              Being a freelance web developer and web designer, I help individuals and startups uplift their online
              presence. Whether you want a <span className="bold">brand new site</span> or{" "}
              <span className="bold">website design</span>, I am your guy. I can also help in boosting the{" "}
              <span className="bold">website SEO</span> and managing <span className="bold">web hosting</span> services
              for you. Explore{" "}
              <Link href="/services">
                <span className="bold page_link">services</span>
              </Link>{" "}
              page for concise details, I am just one message away.
            </h2>
          </div>
        </div>
        <div className="my-container">
          <div className="row servicesHome-row">
            {servicesHome.map((x, i) => (
              <div key={i} className="col-md-4 col-12 servicesHome-single">
                <ServiceCard image={x.image} title={x.title} description={x.description} />
              </div>
            ))}
          </div>
        </div>
      </div>
    </StyledServicesHome>
  );
}
