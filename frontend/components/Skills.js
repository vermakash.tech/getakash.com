import React from 'react'
import { useSelector } from 'react-redux'
import StyledSkills from '../styles/components/Skills.styled'
import SkillCard from "./_SkillCard";

export default function Skills({ fillBackground }) {
  const skills = useSelector((state) => state.skills.data);

  return (
    <StyledSkills fillBackground={fillBackground}>
      <div className="my-container">
        <div className="skills row">
          {skills.map((x, i) => (
            <div key={i} className="skills-single col-sm-3 col-6">
              <SkillCard id={i} img_link={x.logo} alt={x.name} />
            </div>
          ))}
        </div>
      </div>
    </StyledSkills>
  );
}
