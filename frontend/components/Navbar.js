import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Link from 'next/link'
import Script from "next/script";
import "bootstrap/dist/css/bootstrap.min.css";
import { useRouter } from "next/router";
import StyledNavbar from "../styles/components/Navbar.styled";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faXmark } from "@fortawesome/free-solid-svg-icons";
import LogoBrand from "./_LogoBrand";
import { toggleIsProjectFlow } from "../redux/slices/projectFlowSlice";

export default function Navbar() {
  const router = useRouter();
  const dispatch = useDispatch();
  const [screenWidth, setScreenWidth] = useState(800);
  const [toggleMenu, setToggleMenu] = useState(false);

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    const changeWidth = () => {
      setScreenWidth(window.innerWidth);
    };
    window.addEventListener("resize", changeWidth);
  }, []);

  const handleToggleNav = () => {
    setToggleMenu(!toggleMenu);
  };
  const closeNav = () => {
    setToggleMenu(false);
  };
  const showProjectRequest = () => {
    useDispatch(toggleIsProjectFlow());
  };

  const isHome =
    !router.asPath.includes("about") &&
    !router.asPath.includes("projects") &&
    !router.asPath.includes("services") &&
    !router.asPath.includes("blogs") &&
    !router.asPath.includes("recommend") &&
    !router.asPath.includes("admin");

  return (
    <StyledNavbar isMobile={toggleMenu}>
      {screenWidth > 992 ? (
        <div className="my-container navbar">
          <div className="navbar_logo">
            <Link href="/">
              <a>
                <LogoBrand className="navbar_logo_img" />
              </a>
            </Link>
          </div>
          <div className="navbar_container">
            <div className="navbar_container_links">
              <Link href="/">
                <a className={`navbar_link ${isHome ? "navbar_link-active" : ""}`}>Home</a>
              </Link>
              <Link href="#footer_contact">
                <a className={`navbar_link`}>Contact</a>
              </Link>
              <Link href="/about">
                <a className={`navbar_link ${router.asPath.includes("about") ? "navbar_link-active" : ""}`}>About</a>
              </Link>
              <Link href="/projects">
                <a className={`navbar_link ${router.asPath.includes("projects") ? "navbar_link-active" : ""}`}>
                  Projects
                </a>
              </Link>
              <Link href="/services">
                <a className={`navbar_link ${router.asPath.includes("services") ? "navbar_link-active" : ""}`}>
                  Services
                </a>
              </Link>
              {/* <Link href="/services">
                <a className={`navbar_link ${router.asPath.includes("blogs") ? "navbar_link-active" : ""}`}>Blogs</a>
              </Link> */}
            </div>
            <a onClick={() => dispatch(toggleIsProjectFlow())} className="navbar_cta">
              Project Request
            </a>
          </div>
        </div>
      ) : (
        <div className="my-container navbar">
          <div className="navbar_logo">
            <Link href="/">
              <a>
                <LogoBrand className="navbar_logo_img" />
              </a>
            </Link>
          </div>
          <div className="navbar_container">
            <a onClick={() => dispatch(toggleIsProjectFlow())} className="navbar_cta">
              Project Request
            </a>
            {toggleMenu ? (
              <FontAwesomeIcon
                className="navbar_hamburger navbar_cross"
                onClick={() => handleToggleNav()}
                icon={faXmark}
              />
            ) : (
              <FontAwesomeIcon className="navbar_hamburger" onClick={() => handleToggleNav()} icon={faBars} />
            )}
          </div>
          <div className={`navbar_container_links ${toggleMenu ? "" : "navbar_container_links_closed"}`}>
            <Link href="/">
              <a onClick={() => closeNav()} className={`navbar_link ${isHome ? "navbar_link-active" : ""}`}>
                Home
              </a>
            </Link>
            <Link href="#footer_contact">
              <a onClick={() => closeNav()} className={`navbar_link`}>
                Contact
              </a>
            </Link>
            <Link href="/about">
              <a
                onClick={() => closeNav()}
                className={`navbar_link ${router.asPath.includes("about") ? "navbar_link-active" : ""}`}
              >
                About
              </a>
            </Link>
            <Link href="/projects">
              <a
                onClick={() => closeNav()}
                className={`navbar_link ${router.asPath.includes("projects") ? "navbar_link-active" : ""}`}
              >
                Projects
              </a>
            </Link>
            <Link href="/services">
              <a
                onClick={() => closeNav()}
                className={`navbar_link ${router.asPath.includes("services") ? "navbar_link-active" : ""}`}
              >
                Services
              </a>
            </Link>
            {/* <Link href="/services">
              <a
                onClick={() => closeNav()}
                className={`navbar_link ${router.asPath.includes("blogs") ? "navbar_link-active" : ""}`}
              >
                Blogs
              </a>
            </Link> */}
          </div>
        </div>
      )}
    </StyledNavbar>
  );
}
