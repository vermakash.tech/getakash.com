import React from "react";
import StyledLoader from "../styles/components/Loader.styled";

export default function Loader() {
  return (
    <StyledLoader>
      <div className="loader3">
        <span></span>
        <span></span>
      </div>
    </StyledLoader>
  );
}
