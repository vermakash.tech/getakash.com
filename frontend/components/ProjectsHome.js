import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import StyledProjectsHome from "../styles/components/ProjectsHome.styled";
import ProjectCard from "./_ProjectCard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { faArrowUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";
import SiteLink from "./_SiteLink";

export default function ProjectsHome() {
  const [focusedCardId, setFocusedCardId] = useState(5);
  const [pvsCardId, setPvsCardId] = useState(2);
  const projects = useSelector((state) => state.projects.data);
  const cardsData = projects.filter((x) => x.pinned);
  const [screenWidth, setScreenWidth] = useState(1200);

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    const changeWidth = () => {
      setScreenWidth(window.innerWidth);
    };
    window.addEventListener("resize", changeWidth);
  }, []);

  const handleCardClick = (event) => {
    let cardId = event.currentTarget.id;
    setPvsCardId(focusedCardId);
    setFocusedCardId(Number(cardId.split("-")[1]));
  };

  let cardArray = [];
  for (let i = 1; i <= cardsData.length; i++) {
    if (i == focusedCardId) {
      if (screenWidth <= 576 && focusedCardId > pvsCardId) {
        cardArray.push(
          <div className="col" key={i}>
            <ProjectCard
              urlLive={cardsData[i - 1].urlLive}
              urlCode={cardsData[i - 1].urlCode}
              desc={cardsData[i - 1].description}
              animation="rotate_90_from_bottom"
              id={`proj-${i}`}
              bg_image={cardsData[i - 1].image}
              title={cardsData[i - 1].title}
            />
          </div>
        );
      } else if (screenWidth <= 576 && focusedCardId < pvsCardId) {
        cardArray.push(
          <div className="col" key={i}>
            <ProjectCard
              urlLive={cardsData[i - 1].urlLive}
              urlCode={cardsData[i - 1].urlCode}
              desc={cardsData[i - 1].description}
              animation="rotate_90_from_top"
              id={`proj-${i}`}
              bg_image={cardsData[i - 1].image}
              title={cardsData[i - 1].title}
            />
          </div>
        );
      } else if (screenWidth > 576 && focusedCardId > pvsCardId) {
        cardArray.push(
          <div className="col" key={i}>
            <ProjectCard
              urlLive={cardsData[i - 1].urlLive}
              urlCode={cardsData[i - 1].urlCode}
              desc={cardsData[i - 1].description}
              animation="rotate_90_from_right"
              id={`proj-${i}`}
              bg_image={cardsData[i - 1].image}
              title={cardsData[i - 1].title}
            />
          </div>
        );
      } else {
        cardArray.push(
          <div className="col" key={i}>
            <ProjectCard
              urlLive={cardsData[i - 1].urlLive}
              urlCode={cardsData[i - 1].urlCode}
              desc={cardsData[i - 1].description}
              animation="rotate_90_from_left"
              id={`proj-${i}`}
              bg_image={cardsData[i - 1].image}
              title={cardsData[i - 1].title}
            />
          </div>
        );
      }
    } else {
      cardArray.push(
        <div className="col" key={i}>
          <div id={`proj-${i}`} onClick={(e) => handleCardClick(e)} className="projects_home-reduced_card">
            <span className="projects_home-reduced_card-content">{cardsData[i - 1].title}</span>
          </div>
        </div>
      );
    }
  }

  return (
    <StyledProjectsHome>
      <div className="my-container projects_home">
        <h1 className="projects_home-heading">
          My Previous Works&nbsp;
          <Link href="/projects">
            <FontAwesomeIcon className="projects_home-heading-redirect" icon={faArrowUpRightFromSquare} />
          </Link>
        </h1>
        <div className="projects_home-container">{cardArray}</div>
        {/* <SiteLink myContainer={true} link="#" title="More Projects" /> */}
      </div>
    </StyledProjectsHome>
  );
}
