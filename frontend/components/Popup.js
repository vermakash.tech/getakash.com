import React from "react";
import StyledPopup from "../styles/components/Popup.styled";
import { closePopup } from "../redux/slices/popupSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faXmark } from "@fortawesome/free-solid-svg-icons";
import { useDispatch } from "react-redux";

export default function Popup({ message, type }) {
  const dispatch = useDispatch();
  const handleClosePopup = () => {
    dispatch(closePopup());
  };
  return (
    <StyledPopup type={type}>
      <div className="popup">
        <FontAwesomeIcon className="popup-icon" icon={type == "SUCCESS" || type == "THANK YOU" ? faCheck : faXmark} />
        <h1 className="popup-type">{type}</h1>
        <h2 className="popup-message">{message}</h2>
        <button onClick={() => handleClosePopup()} className="popup-close">
          Ok
        </button>
      </div>
    </StyledPopup>
  );
}
