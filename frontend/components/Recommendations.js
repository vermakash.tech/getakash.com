import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import StyledRecommendations from "../styles/components/Recommendations.styled";
import RecommendationCard from "./_RecommendationCard";

export default function Recommendations() {
  const [selectedRecomm, setSelectedRecomm] = useState(0);
  const recomms = useSelector((state) => state.recommendations.data);
  const handlePaginateClick = (event) => {
    const clickedId = event.currentTarget.id;
    const selectedId = Number(clickedId.split("_")[2]);
    setSelectedRecomm(selectedId);
  };
  return (
    <StyledRecommendations className="my-container recommendations">
      <h1 className="recommendations-heading">What do people say?</h1>
      <RecommendationCard
        key={selectedRecomm}
        desc={recomms[selectedRecomm].description}
        name={recomms[selectedRecomm].name}
        profession={recomms[selectedRecomm].profession}
        profile={recomms[selectedRecomm].profile}
      />
      <div className="recommendations-paginate_container">
        <div className="recommendations-paginate">
          {recomms.map((x, i) => (
            <span
              id={`recomm_pagin_${i}`}
              key={`recomm_pagin_${i}`}
              className={`recommendations-paginate-box ${
                selectedRecomm == i ? "recommendations-paginate-box-active" : ""
              }`}
              onClick={(e) => handlePaginateClick(e)}
            ></span>
          ))}
        </div>
      </div>
    </StyledRecommendations>
  );
}
