import React from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleUser } from "@fortawesome/free-solid-svg-icons";

export default function RecommendationCard({ desc, name, profession, profile }) {
  return (
    <div className="recommendation_card">
      <div className="recommendation_card-container">
        <h3 className="recommendation_card-desc">{desc}</h3>
        <a href={profile} target="_blank" rel="noopener noreferrer" className="recommendation_card-name">
          <h2 className="recommendation_card-name">
            {name}
            <FontAwesomeIcon className="recommendation_card-user" icon={faCircleUser} />
          </h2>
        </a>
        <h3 className="recommendation_card-profession">
          <span></span> {profession}
        </h3>
      </div>
    </div>
  );
}
