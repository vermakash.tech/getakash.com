import React, { useEffect, useState } from "react";
import StyledServicesCurved from "../styles/components/ServicesCurved.styled";

export default function ServicesCurved({ layoutText, image, heading, content }) {
  const [screenWidth, setScreenWidth] = useState(1200);

  useEffect(() => {
    setScreenWidth(window.innerWidth);
    const changeWidth = () => {
      setScreenWidth(window.innerWidth);
    };
    window.addEventListener("resize", changeWidth);
  }, []);
  return (
    <StyledServicesCurved className="services_curved" image={image}>
      {layoutText == "left" && screenWidth > 576 ? (
        <div className="row services_curved-row">
          <div className="col-sm-6 col-8 services_curved-text_col">
            <h1>{heading}</h1>
            <h3>{content}</h3>
          </div>
          <div className="col-sm-6 col-4 services_curved-img_col services_curved-img_col-right"></div>
        </div>
      ) : (
        <div className="row services_curved-row">
          <div className="col-sm-6 col-4 services_curved-img_col services_curved-img_col-left"></div>
          <div className="col-sm-6 col-8 services_curved-text_col">
            <h1>{heading}</h1>
            <h3>{content}</h3>
          </div>
        </div>
      )}
    </StyledServicesCurved>
  );
}
