import React from 'react'

export default function SkillCard({img_link, alt, id}) {
  return (
    <div id={`skill-${id}`} className='skillCard'>
        <img src={img_link} alt={alt} title={alt}></img>
    </div>
  )
}
