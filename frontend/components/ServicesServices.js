import React from "react";
import { useSelector } from "react-redux";
import StyledServicesServices from "../styles/components/ServicesServices.styled";
import ServicesCurved from "./_ServicesCurved";

export default function ServicesServices() {
  const services = useSelector((state) => state.services.data);
  return (
    <StyledServicesServices className="my-container services_services">
      <h1 className="services_services-heading">Professional Services</h1>
      {services.map((x, i) => {
        if ((i + 1) % 2 == 0) {
          return <ServicesCurved key={i} layoutText="right" image={x.image} heading={x.title} content={x.description} />;
        } else {
          return <ServicesCurved key={i} layoutText="left" image={x.image} heading={x.title} content={x.description} />;
        }
      })}
    </StyledServicesServices>
  );
}
