import React from 'react'
import Link from 'next/link'
import { useDispatch } from "react-redux";
import { toggleIsProjectFlow } from "../redux/slices/projectFlowSlice";

export default function LandingCTA() {
  const dispatch = useDispatch();
  return (
    <button onClick={() => dispatch(toggleIsProjectFlow())} className="landing-left-cta_button">
      Make a Project&nbsp;Request
    </button>
  );
}
