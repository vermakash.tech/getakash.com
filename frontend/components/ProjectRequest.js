import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import StyledProjectRequest from "../styles/components/ProjectRequest.styled";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark, faRightLong, faLeftLong } from "@fortawesome/free-solid-svg-icons";
import { toggleIsProjectFlow } from "../redux/slices/projectFlowSlice";
import axios from "axios";
import { sendEmailPR, sendEmailPRMe } from "../../email";

export default function ProjectRequest({ isProjectFlow, setIsProjectFlow }) {
  const dispatch = useDispatch();
  const [projectType, setProjectType] = useState(
    localStorage.getItem("projectType") ? localStorage.getItem("projectType") : null
  );
  const [websiteType, setWebsiteType] = useState(
    localStorage.getItem("websiteType") ? localStorage.getItem("websiteType") : null
  );
  const [techStack, setTechStack] = useState(
    localStorage.getItem("techStack") ? localStorage.getItem("techStack") : null
  );
  const [seoService, setSeoService] = useState(
    localStorage.getItem("seoService") ? localStorage.getItem("seoService") : null
  );
  const [hostingService, setHostingService] = useState(
    localStorage.getItem("hostingService") ? localStorage.getItem("hostingService") : null
  );
  const [timeLimit, setTimeLimit] = useState(
    localStorage.getItem("timeLimit") ? localStorage.getItem("timeLimit") : null
  );
  const [email, setEmail] = useState(localStorage.getItem("email") ? localStorage.getItem("email") : null);
  const [contact, setContact] = useState(localStorage.getItem("contact") ? localStorage.getItem("contact") : null);
  const [name, setName] = useState(localStorage.getItem("name") ? localStorage.getItem("name") : null);
  const [language, setLanguage] = useState(localStorage.getItem("language") ? localStorage.getItem("language") : null);
  const [additionalInfo, setAdditionalInfo] = useState(
    localStorage.getItem("additionalInfo") ? localStorage.getItem("additionalInfo") : null
  );
  const [nextStep, setNextStep] = useState(
    localStorage.getItem("nextStep") ? Number(localStorage.getItem("nextStep")) : 1
  );
  const [totalSteps, setTotalSteps] = useState(
    localStorage.getItem("totalSteps") ? Number(localStorage.getItem("totalSteps")) : 5
  );
  const [isFinished, setIsFinished] = useState(false);

  const handleCrossClick = () => {
    dispatch(toggleIsProjectFlow());
    if (isFinished) {
      setIsFinished(false);
      setNextStep(1);
      setTotalSteps(5);
      localStorage.removeItem("projectType");
      localStorage.removeItem("websiteType");
      localStorage.removeItem("timeLimit");
      localStorage.removeItem("techStack");
      localStorage.removeItem("seoService");
      localStorage.removeItem("hostingService");
      localStorage.removeItem("name");
      localStorage.removeItem("language");
      localStorage.removeItem("email");
      localStorage.removeItem("contact");
      localStorage.removeItem("additionalInfo");
      localStorage.removeItem("nextStep");
      localStorage.removeItem("totalSteps");
    }
  };
  const handleChange = (event, type) => {
    if (type == "projectType") {
      setProjectType(event.target.value);
    } else if (type == "websiteType") {
      setWebsiteType(event.target.value);
    } else if (type == "timeLimit") {
      setTimeLimit(event.target.value);
    } else if (type == "techStack") {
      setTechStack(event.target.value);
    } else if (type == "seoService") {
      setSeoService(event.target.value);
    } else if (type == "name") {
      setName(event.target.value);
    } else if (type == "language") {
      setLanguage(event.target.value);
    } else if (type == "email") {
      setEmail(event.target.value);
    } else if (type == "contact") {
      setContact(event.target.value);
    } else if (type == "additionalInfo") {
      setAdditionalInfo(event.target.value);
    } else if (type == "hostingService") {
      setHostingService(event.target.value);
    }
  };
  const handleNext = async (type) => {
    let currentStep = nextStep;
    if (type == "projectType") {
      if (projectType) {
        localStorage.setItem("projectType", projectType);
        setNextStep(currentStep + 1);
        localStorage.setItem("nextStep", nextStep + 1);
        if (projectType == "web_design" || projectType == "other" || projectType == "website_upgrade") {
          setTotalSteps(5);
          localStorage.setItem("totalSteps", 5);
        } else {
          setTotalSteps(6);
          localStorage.setItem("totalSteps", 6);
        }
      } else {
        alert("You have to select a project type to continue.");
      }
    } else if (type == "websiteType&timeLimit") {
      if (websiteType && timeLimit) {
        localStorage.setItem("websiteType", websiteType);
        localStorage.setItem("timeLimit", timeLimit);
        setNextStep(currentStep + 1);
        localStorage.setItem("nextStep", nextStep + 1);
      } else {
        alert("You have to select a website type and time limit to proceed.");
      }
    } else if (type == "techStack") {
      if (techStack) {
        localStorage.setItem("techStack", techStack);
      }
      setNextStep(currentStep + 1);
      localStorage.setItem("nextStep", nextStep + 1);
    } else if (type == "seoService") {
      if (seoService) {
        localStorage.setItem("seoService", seoService);
      }
      setNextStep(currentStep + 1);
      localStorage.setItem("nextStep", nextStep + 1);
    } else if (type == "hostingService") {
      if (hostingService) {
        localStorage.setItem("hostingService", hostingService);
      }
      setNextStep(currentStep + 1);
      localStorage.setItem("nextStep", nextStep + 1);
    } else if (type == "name&Language") {
      if (name && language) {
        localStorage.setItem("name", name);
        localStorage.setItem("language", language);
        setNextStep(currentStep + 1);
        localStorage.setItem("nextStep", nextStep + 1);
      } else {
        alert("You have to input your name and language to proceed.");
      }
    } else if (type == "email&contact") {
      if (email && contact) {
        if (validateEmail(email)) {
          localStorage.setItem("email", email);
          localStorage.setItem("contact", contact);
          setNextStep(currentStep + 1);
          localStorage.setItem("nextStep", nextStep + 1);
        } else {
          alert("You have to input correct email address.");
        }
      } else {
        alert("You have to input your email and contact number to proceed.");
      }
    } else if (type == "additionalInfo") {
      if (additionalInfo) {
        localStorage.setItem("additionalInfo", additionalInfo);
      }
      setNextStep(currentStep + 1);
      localStorage.setItem("nextStep", nextStep + 1);
      try {
        let body = {};
        if (projectType) body["projectType"] = projectType;
        if (websiteType) body["websiteType"] = websiteType;
        if (timeLimit) body["timeLimit"] = timeLimit;
        if (techStack) body["techStack"] = techStack;
        if (seoService) body["seoService"] = seoService;
        if (hostingService) body["hostingService"] = hostingService;
        if (name) body["name"] = name;
        if (language) body["language"] = language;
        if (email) body["email"] = email;
        if (contact) body["contact"] = contact;
        if (additionalInfo) body["additionalInfo"] = additionalInfo;
        console.log("body ----->>>>>", body);
        const projectReqData = await axios.post("http://54.219.113.253/api/initials/projectrequest", body, {
          headers: {
            "Content-Type": "application/json;charset=UTF-8",
            "Access-Control-Allow-Origin": "*",
          },
        });
        if (projectReqData) {
          setIsFinished(true);
          let desc = JSON.stringify(body);
          let description = desc.split(",").join("\n");
          let contentMe = {
            name: body.name,
            email: body.email,
            description: description,
          };

          const sendEmail = await sendEmailPR({
            name: body.name,
            email: body.email,
          });
          const sendEmailMe = await sendEmailPRMe(contentMe);
          sendEmail();
          sendEmailMe();
        }
      } catch (error) {
        console.log(error);
        alert("Something went wrong");
      }
    }
  };
  const handleBack = () => {
    let pvsStep = nextStep - 1;
    setNextStep(pvsStep);
  };
  function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
  let toShow;
  if (nextStep == 1) {
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label">What type of project do you want? &#9733;</label>
        <select
          onChange={(evt) => handleChange(evt, "projectType")}
          className="project_request-chunk-select"
          name="projectType"
          id="projectType"
          value={projectType ? projectType : ""}
        >
          <option disabled selected value="">
            select an option
          </option>
          <option value="web_design">Web Design</option>
          <option value="web_development">Web Development</option>
          <option value="full_website">Full Website</option>
          <option value="seo_boost">Seo Boost</option>
          <option value="web_hosting">Web Hosting</option>
          <option value="website_upgrade">Website Upgrade</option>
          <option value="other">Other</option>
        </select>
        <div className="project_request-chunk-actions">
          <h3
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 onClick={() => handleNext("projectType")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (nextStep == 2) {
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label">Please specify the niche of the website? &#9733;</label>
        <select
          onChange={(evt) => handleChange(evt, "websiteType")}
          className="project_request-chunk-select"
          name="websiteType"
          id="websiteType"
          value={websiteType ? websiteType : ""}
        >
          <option disabled selected value="">
            select an option
          </option>
          <option value="portfolio">Portfolio</option>
          <option value="ecommerce">E-commerce</option>
          <option value="fashion">Fashion</option>
          <option value="real_estate">Real Estate</option>
          <option value="photography">Photography</option>
          <option value="food">Food</option>
          <option value="saas">Saas</option>
          <option value="fitness">Fitness</option>
          <option value="blog">Blog</option>
          <option value="other">Other</option>
        </select>
        <label className="project_request-chunk-label">What is the approx time limit for the project? &#9733;</label>
        <input
          onChange={(evt) => handleChange(evt, "timeLimit")}
          className="project_request-chunk-select"
          placeholder="no. of weeks"
          name="timeLimit"
          id="timeLimit"
          value={timeLimit ? timeLimit : ""}
        ></input>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("websiteType&timeLimit")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (nextStep == 3 && (projectType == "web_development" || projectType == "full_website")) {
    //techStack
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label project_request-chunk-label-single">
          Do you have any specific technology in mind?
        </label>
        <input
          onChange={(evt) => handleChange(evt, "techStack")}
          className="project_request-chunk-select"
          placeholder="eg. nodejs, mysql, etc"
          name="techStack"
          id="techStack"
          value={techStack ? techStack : ""}
        ></input>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("techStack")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (nextStep == 3 && projectType == "seo_boost") {
    //type of seo boost & disclaimer footer
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label project_request-chunk-label-single">
          What type of seo service do you want?
        </label>
        <select
          onChange={(evt) => handleChange(evt, "seoService")}
          className="project_request-chunk-select"
          name="seoService"
          id="seoService"
          value={seoService ? seoService : ""}
        >
          <option disabled selected value="">
            select an option
          </option>
          <option value="keyword_research">Keyword Research</option>
          <option value="onpage_seo">Onpage Seo</option>
          <option value="backlinks">Backlinks</option>
          <option value="complete">Complete</option>
        </select>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("seoService")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (nextStep == 3 && projectType == "web_hosting") {
    //which hosting service
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label project_request-chunk-label-single">
          Do you have any specific hosting service in mind?
        </label>
        <input
          onChange={(evt) => handleChange(evt, "hostingService")}
          className="project_request-chunk-select"
          placeholder="eg. aws, hostinger, etc"
          name="hostingService"
          id="hostingService"
          value={hostingService ? hostingService : ""}
        ></input>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("hostingService")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (
    (nextStep == 3 && (projectType == "web_design" || projectType == "website_upgrade" || projectType == "other")) ||
    (nextStep == 4 && !(projectType == "web_design" || projectType == "website_upgrade" || projectType == "other"))
  ) {
    //name and language
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label">What is your name? &#9733;</label>
        <input
          onChange={(evt) => handleChange(evt, "name")}
          className="project_request-chunk-select"
          placeholder=""
          name="name"
          id="name"
          value={name ? name : ""}
        ></input>
        <label className="project_request-chunk-label">In which language are you most comfortable? &#9733;</label>
        <select
          onChange={(evt) => handleChange(evt, "language")}
          className="project_request-chunk-select"
          name="language"
          id="language"
          value={language ? language : ""}
        >
          <option disabled selected value="">
            select an option
          </option>
          <option value="english">English</option>
          <option value="hindi">Hindi</option>
          <option value="both">Both</option>
        </select>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("name&Language")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (
    (nextStep == 4 && (projectType == "web_design" || projectType == "website_upgrade" || projectType == "other")) ||
    (nextStep == 5 && !(projectType == "web_design" || projectType == "website_upgrade" || projectType == "other"))
  ) {
    //email and contact
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label">What is your email? &#9733;</label>
        <input
          onChange={(evt) => handleChange(evt, "email")}
          className="project_request-chunk-select"
          placeholder=""
          type="email"
          name="email"
          id="email"
          value={email ? email : ""}
        ></input>
        <label className="project_request-chunk-label">What is your whatsapp/contact number? &#9733;</label>
        <input
          onChange={(evt) => handleChange(evt, "contact")}
          className="project_request-chunk-select"
          placeholder=""
          name="contact"
          id="contact"
          type="number"
          value={contact ? contact : ""}
        ></input>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("email&contact")} className="project_request-chunk-actions-next">
            Next
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (
    (nextStep == 5 && (projectType == "web_design" || projectType == "website_upgrade" || projectType == "other")) ||
    (nextStep == 6 && !(projectType == "web_design" || projectType == "website_upgrade" || projectType == "other"))
  ) {
    //further information and submit
    toShow = (
      <div className="project_request-chunk">
        <label className="project_request-chunk-label">Do you have any additional information?</label>
        <textarea
          onChange={(evt) => handleChange(evt, "additionalInfo")}
          className="project_request-chunk-textarea"
          placeholder=""
          name="additionalInfo"
          id="additionalInfo"
          value={additionalInfo ? additionalInfo : ""}
        ></textarea>
        <div className="project_request-chunk-actions">
          <h3
            onClick={() => handleBack()}
            className={`project_request-chunk-actions-back ${
              nextStep == 1 ? "project_request-chunk-actions-inactive" : ""
            }`}
          >
            <FontAwesomeIcon className="project_request-chunk-actions-back-icon" icon={faLeftLong} />
            Back
          </h3>
          <h3 className="project_request-chunk-actions-next">{`${nextStep} / ${totalSteps}`}</h3>
          <h3 onClick={() => handleNext("additionalInfo")} className="project_request-chunk-actions-next">
            Submit
            <FontAwesomeIcon className="project_request-chunk-actions-next-icon" icon={faRightLong} />
          </h3>
        </div>
      </div>
    );
  } else if (isFinished) {
    toShow = (
      <div className="project_request-chunk project_request-chunk_success">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
          <circle
            className="path circle"
            fill="none"
            stroke="#FBFCFF"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <polyline
            className="path check"
            fill="none"
            stroke="#FBFCFF"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            points="100.2,40.2 51.5,88.8 29.8,67.5 "
          />
        </svg>
        <p className="success">Requst sent successfully! I will contact you within 36 hours</p>
      </div>
    );
  }
  return (
    <StyledProjectRequest className="project_request">
      <div className="project_request-container">
        <FontAwesomeIcon onClick={() => handleCrossClick()} className="project_request-cross" icon={faXmark} />
        {nextStep == 1 ? <h2 className="project_request-hey">Hey👋</h2> : null}
        {toShow}
      </div>
    </StyledProjectRequest>
  );
}
