import React from "react";
import Link from "next/link";
import StyledProjectCard from "../styles/components/ProjectCard.styled";

export default function ProjectCard({ bg_image, title, animation, isShadow, mainAnimation, desc, urlLive, urlCode }) {
  return (
    <StyledProjectCard
      bg_image={bg_image}
      is_reduced={false}
      animation_name={animation}
      isShadow={isShadow ? true : false}
      mainAnimation={mainAnimation}
    >
      <div className="project_card project_card-img_container_wrapper">
        <div className="project_card-img_container">
          {/* <img src="/images/project_nexter.jpg" className="project_card-img_container-img"></img> */}
        </div>
        {/* <a href={urlLive} target="_blank" rel="noopener noreferrer"> */}
          <h1 className="project_card-title">{title}</h1>
        {/* </a> */}
      </div>
      <div className="project_card project_card-body">
        <h3>{desc}</h3>
        <div className="project_card-body-links_container">
          <a
            href={urlCode}
            target="_blank"
            rel="noopener noreferrer"
            className={`project_card-body-link ${urlCode ? "" : "project_card-body-link-inactive"}`}
          >
            Code
          </a>
          {urlLive && <a href={urlLive} target="_blank" rel="noopener noreferrer" className="project_card-body-link">
            Live
          </a>}
        </div>
      </div>
    </StyledProjectCard>
  );
}
