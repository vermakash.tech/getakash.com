import React from "react";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faXmark } from "@fortawesome/free-solid-svg-icons";

export default function ServiceCard({ image, title, description }) {
  const [state, setState] = useState({
    isCardExpanded: false,
  });

  const handleCardExpansion = () => {
    setState({
      ...state,
      isCardExpanded: !state.isCardExpanded,
    });
  };

  return (
    <div className="servicecard">
      <div className="servicecard-container">
        <img className="servicecard-img" src={image} alt={title} title={title}></img>
        <div className="servicecard-body">
          <button onClick={() => handleCardExpansion()} className="servicecard-body-button">
            {state.isCardExpanded ? (
              <FontAwesomeIcon className="servicecard-body-button-cross" icon={faXmark} />
            ) : (
              <FontAwesomeIcon className="servicecard-body-button-plus" icon={faPlus} />
            )}
          </button>
          <h1 className="servicecard-body-heading">{title}</h1>
          <h2
            key={state.isCardExpanded ? "open" : "close"}
            className={`servicecard-body-desc ${
              state.isCardExpanded ? "servicecard-body-desc-open" : "servicecard-body-desc-hidden"
            }`}
          >{`${description.substr(0, 150)}...`}</h2>
        </div>
      </div>
    </div>
  );
}
