import React from "react";

export default function FaqCard({ ques, ans }) {
  return (
    <div className="faq_card">
      <div className="faq_card-side faq_card-side-front">{ques}</div>
      <div className="faq_card-side faq_card-side-back">{ans}</div>
    </div>
  );
}
