import React, { useState, useRef } from "react";
import StyledFooter from "../styles/components/Footer.styled";
import Link from "next/link";
import { closePopup, openPopup, setPopupData } from "../redux/slices/popupSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faPhone, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { faLinkedin, faWhatsapp, faInstagram } from "@fortawesome/free-brands-svg-icons";
import { useDispatch } from "react-redux";
import { sendEmailCF } from "../../email";

export default function Footer() {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [description, setDescription] = useState("");
  const form = useRef();
  const handleSendEmail = async (event) => {
    event.preventDefault();
    //send email
    try {
      const sendEmail = await sendEmailCF(form.current);
      const emailRes = sendEmail();
      if (emailRes) {
        dispatch(setPopupData({ msg: "Email sent Successfully", type: "SUCCESS" }));
        dispatch(openPopup());
      }
    } catch (error) {
      alert("Something went wrong");
      console.log(error);
    }
    setName("");
    setEmail("");
    setDescription("");
  };
  return (
    <StyledFooter>
      <div className="footer">
        <div className="my-container">
          <div className="row footer-row">
            <div className="footer-column footer-column-left col-md-6 col-12">
              <img className="footer-column-left-logo" src="/images/logoFooter.svg"></img>
            </div>
            <div className="footer-column footer-column-right col-md-6 col-12">
              <div className="row footer-column-right-row">
                <div className="col-4 footer-column-right-link_container">
                  <Link href="/">
                    <h2 className="page_link footer-column-right-link">Home</h2>
                  </Link>
                  <Link href="/about">
                    <h2 className="page_link footer-column-right-link">About</h2>
                  </Link>
                  <Link href="resume_akash_verma.pdf">
                    <h2 className="page_link footer-column-right-link">Resume</h2>
                  </Link>
                  <Link href="/projects">
                    <h2 className="page_link footer-column-right-link">Projects</h2>
                  </Link>
                </div>
                <div className="col-4 footer-column-right-link_container">
                  <Link href="#footer_contact">
                    <h2 className="page_link footer-column-right-link">Contact</h2>
                  </Link>
                  <a href="/services#faq">
                    <h2 className="page_link footer-column-right-link">F.A.Q</h2>
                  </a>
                  <h2 className="page_link footer-column-right-link">Blogs</h2>
                  <Link href="/admin">
                    <h2 className="page_link footer-column-right-link">Admin</h2>
                  </Link>
                </div>
                <div className="col-4 footer-column-right-link_container">
                  <Link href="/services">
                    <h2 className="page_link footer-column-right-link">Services</h2>
                  </Link>
                  <Link href="/about">
                    <h2 className="page_link footer-column-right-link">Education</h2>
                  </Link>
                  <Link href="/about">
                    <h2 className="page_link footer-column-right-link">Experience</h2>
                  </Link>
                  <Link href="/recommend">
                    <h2 className="page_link footer-column-right-link">Recommend</h2>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-mini">
          <div className="footer-mini-container my-container">
            <div className="footer-mini-link_container">
              <a href="mailto:vermakash.tech@gmail.com" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="footer-mini-link" icon={faEnvelope} />
              </a>
              <a href="https://www.linkedin.com/in/getakash/" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="footer-mini-link" icon={faLinkedin} />
              </a>
              <a href="https://api.whatsapp.com/send?phone=7054443300" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="footer-mini-link" icon={faWhatsapp} />
              </a>
              <a href="https://www.instagram.com/vermakash.1.2.9/" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="footer-mini-link" icon={faInstagram} />
              </a>
              <a href="tel:+91-7054443300" target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="footer-mini-link" icon={faPhone} />
              </a>
            </div>
            <div className="footer-mini-copyright">copyright @getakash.com</div>
          </div>
        </div>
        <div id="footer_contact" className="footer-contact">
          <form ref={form} onSubmit={(e) => handleSendEmail(e)} className="footer-contact-form">
            <div className="footer-contact-form_container">
              <input
                onChange={(e) => setName(e.target.value)}
                className="footer-contact-input"
                placeholder="Name"
                name="name"
                value={name}
                required
              />
              <input
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                className="footer-contact-input"
                placeholder="Email"
                name="email"
                value={email}
                required
              />
            </div>
            <div className="footer-contact-form_container footer-contact-form_container-md">
              <textarea
                onChange={(e) => setDescription(e.target.value)}
                className="footer-contact-textarea"
                placeholder="Description"
                name="description"
                value={description}
                required
              />
            </div>
            <div className="footer-contact-form_container footer-contact-form_container-md">
              <button type="submit" className="footer-contact-button">
                <FontAwesomeIcon className="footer-contact-arrow_right" icon={faArrowRight} />
              </button>
            </div>
          </form>
        </div>
      </div>
    </StyledFooter>
  );
}

