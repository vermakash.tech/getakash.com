import React from "react";
import { useSelector } from "react-redux";
import StyledLandingServices from "../styles/components/LandingServices.styled";

export default function LandingServices() {
  const { description, tagline } = useSelector((state) => state.initials.data);
  return (
    <StyledLandingServices className="my-container landing_services">
      <div className="row landing_services-row">
        <div className="col-lg-5 col-md-6 col-sm-12 landing_services-img_container">
          <img src="/images/landingService.svg" alt="Services Page" title="Services page of Getakash.com" />
        </div>
        <div className="col-lg-7 col-md-6 col-sm-12 landing_services-content">
          <h1 className="landing_services-content-heading">
            <span className="landing_services-content-heading-primary">{tagline.primary} </span>
            {tagline.secondary}
          </h1>
          <h2 className="landing_services-content-desc">
            <span className="landing_services-content-desc-primary">{description.primary} </span>
            {description.secondary}
          </h2>
        </div>
      </div>
    </StyledLandingServices>
  );
}
