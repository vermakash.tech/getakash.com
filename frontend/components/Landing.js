import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import StyledLanding from '../styles/components/Landing.styled'
import LandingCTA from "./_LandingCTA";
import LandingImage from "./_LandingImage";

export default function Landing() {
  const [state, setState] = useState({
    isTabletOn: true,
    isMonitorOn: true,
  });

  const initials = useSelector((state) => state.initials.data);

  useEffect(() => {
    const tabletButton = document.querySelector(".tablet_button");
    const monitorButton = document.querySelector(".monitor_button");
    const monitorOffScreen = document.querySelector(".monitor_off_screen");
    const tabletOffScreen = document.querySelector(".tablet_off_screen");

    const toggleMonitorDisplayProperty = () => {
      monitorOffScreen.style.display = state.isMonitorOn ? "block" : "none";
      let currState = state.isMonitorOn;
      setState({
        ...state,
        isMonitorOn: !currState,
      });
    };

    const toggleTabletDisplayProperty = () => {
      tabletOffScreen.style.display = state.isTabletOn ? "block" : "none";
      let currState = state.isTabletOn;
      setState({
        ...state,
        isTabletOn: !currState,
      });
    };

    tabletButton.addEventListener("click", toggleTabletDisplayProperty);
    monitorButton.addEventListener("click", toggleMonitorDisplayProperty);

    return () => {
      tabletButton.removeEventListener("click", toggleTabletDisplayProperty);
      monitorButton.removeEventListener("click", toggleMonitorDisplayProperty);
    };
  }, [state]);

  return (
    <StyledLanding>
      <img className="bg_img bg_img-lg bg_img-lg-1" src="/images/landingBgBig.svg"></img>
      <img className="bg_img bg_img-sm" src="/images/landingBgSmall.svg"></img>
      <img className="bg_img bg_img-lg bg_img-lg-2" src="/images/landingBgBig.svg"></img>
      <img className="bg_img bg_img-lg bg_img-lg-3" src="/images/landingBgBig.svg"></img>
      <div className="my-container landing">
        <div className="row">
          <div className="landing-left col-lg-5">
            <h1 className="landing-left-tag">{initials.tagline.primary}</h1>
            <h2 className="landing-left-desc">{initials.description.primary}</h2>
            <LandingCTA />
          </div>
          <div className="col-lg-7 landing-right">
            {/* <ReactSVG evalScripts='once' className='landing-right-image' role="img" src="/images/landingImage3.svg" /> */}
            <LandingImage />
          </div>
        </div>
      </div>
    </StyledLanding>
  );
}
