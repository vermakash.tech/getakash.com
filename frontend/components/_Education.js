import React from "react";

export default function Education({ education }) {
  return (
    <>
      {education.map((x, i) => (
        <div key={i} className="ed">
          <div className="ed-intro">
            <a href={x.profile} target="_blank" rel="noopener noreferrer">
              <img src={x.logo} alt="x.college"></img>
            </a>
            <div>
              <h3>{x.college}</h3>
              <h4>
                <span>{x.course}</span>
                {x.specialization ? <span>{x.specialization}</span> : null}
              </h4>
            </div>
          </div>
          <div className="ed-desc">
            <ul>
              <li>{`${x.from} - ${Number(x.to) > Number(new Date().getFullYear()) ? "Present" : x.to}`}</li>
              {x.grade ? <li>{`${x.gradeType}: ${x.grade}`}</li> : null}
            </ul>
          </div>
        </div>
      ))}
    </>
  );
}
