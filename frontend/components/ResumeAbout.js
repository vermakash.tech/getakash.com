import React from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import StyledResume from "../styles/components/ResumeAbout.styled";
import Education from "./_Education";
import ExperienceCard from "./_Experiences";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFile, faXmark } from "@fortawesome/free-solid-svg-icons";

export default function RessumeAbout() {
  const [isCertiOpen, setIsCertiOpen] = useState(false);
  const [certiUrl, setCertiUrl] = useState("");
  const { experiences, education, certifications } = useSelector((state) => state.aboutMe.data);

  const handleCertiClick = (event) => {
    setCertiUrl(event.currentTarget.id);
    setIsCertiOpen(true);
  };
  const stopEventPropagation = (event) => {
    event.stopPropagation();
  };

  let ulArr = [];
  let liArr = [];
  for (let i = 0; i < certifications.length; i++) {
    liArr.push(
      <li key={i} onClick={(e) => handleCertiClick(e)} id={certifications[i].image}>
        <FontAwesomeIcon className="fileIcon" icon={faFile} />
        {certifications[i].title}
      </li>
    );
    if ((i + 1) % 5 == 0 || i == certifications.length - 1) {
      ulArr.push(liArr);
      liArr = [];
    }
  }

  let certis = ulArr.map((x, i) => (
    <ul key={i} className="cert">
      {x}
    </ul>
  ));
  return (
    <StyledResume className="container-fluid resume_about">
      <div className="row resume_about-row">
        <div className="col-lg-5 col-md-12 resume_about-col">
          <div className=" resume_about-edex--item  resume_about-edex--ex">
            <h1 id="experiences">EXPERIENCE</h1>
            <ExperienceCard experiences={experiences} />
          </div>
        </div>
        <div className="col-lg-5 col-md-12 resume_about-col">
          <div className="resume_about-edex--item resume_about-edex--ed">
            <h1 id="education">EDUCATION</h1>
            <Education education={education} />
          </div>
          <div className="resume_about-edex--item resume_about-edex--cert">
            <h1>CERTIFICATIONS</h1>
            {certis}
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-lg-10 col-md-12 resume_about-col">
            <div className="resume_about-edex--item resume_about-edex--excurr">
              <h1>EXTRA CURRICULAR</h1>
              <ul className="excurr">
                <img src="/images/boxing.png" alt="boxing"></img>
                <div>
                  <li>
                    Was an active member of <span>Boxing</span> Team of IIT (BHU) Varanasi.
                  </li>
                  <li>Secured 1st Position twice in Inter-Department Boxing Tournament. </li>
                </div>
              </ul>
            </div>
          </div>
          <div className="col-lg-10 col-md-12 resume_about-col">
            <div className="resume_about-edex--item resume_about-edex--hob">
              <h1>HOBBIES</h1>
              <ul className="hob">
                <div>
                  <li>
                    <img src="/images/cooking.png" alt="cooking"></img> Cooking
                  </li>
                  <li>
                    <img src="/images/book.png" alt="novels"></img> Novels
                  </li>
                </div>
                <div>
                  <li>
                    <img src="/images/music.png" alt="music"></img> Music
                  </li>
                  <li>
                    <img src="/images/coding.png" alt="coding"></img> Coding
                  </li>
                </div>
                <div>
                  <li>
                    <img src="/images/icon-gym.png" alt="music"></img> Workout
                  </li>
                  <li>
                    <img src="/images/icon-holiday.png" alt="coding"></img> Travel
                  </li>
                </div>
              </ul>
            </div>
          </div>
        </div>
      </div>
      {isCertiOpen ? (
        <div onClick={() => setIsCertiOpen(false)} className="resume_about-certi_img">
          <FontAwesomeIcon
            onClick={() => setIsCertiOpen(false)}
            className="resume_about-certi_img-cross"
            icon={faXmark}
          />
          <div onClick={(e) => stopEventPropagation(e)}>
            <img className="resume_about-certi_img-img" src={certiUrl}></img>
          </div>
        </div>
      ) : null}
    </StyledResume>
  );
}
