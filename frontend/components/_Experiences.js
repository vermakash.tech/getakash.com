import React from "react";

export default function ExperienceCard({ experiences }) {
  return (
    <>
      {experiences.map((x, i) => (
        <div className="ex" key={i}>
          <div className="ex-intro">
            <a href={x.profile} target="_blank" rel="noopener noreferrer">
              <img src={x.logo} alt="x.company"></img>
            </a>
            <div>
              <h3>{x.title}</h3>
              <h4>
                <span>{x.company}</span>
                <span>{x.type}</span>
                <span>{new Date(x.endDate) > new Date() ? "Ongoing" : `${x.duration} ${x.durationType}`}</span>
              </h4>
            </div>
          </div>
          <div className="ex-desc">
            <ul>
              {x.description.map((li, i) => (
                <li key={i}>{li}</li>
              ))}
            </ul>
          </div>
        </div>
      ))}
    </>
  );
}
