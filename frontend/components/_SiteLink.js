import React from "react";
import StyledSiteLink from "../styles/components/SiteLink.styled";
import Link from "next/link";

export default function SiteLink({ link, title, myContainer }) {
  return (
    <StyledSiteLink className={`${myContainer ? "my-container" : ""} site_link`}>
      <div className="wavy-line wavy-line-green" data-text="xxxxxxxxxxxxxx"></div>
      <Link href={link}>
        <div className="site_link-link">{title}</div>
      </Link>
    </StyledSiteLink>
  );
}
