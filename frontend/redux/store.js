import { configureStore } from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'
import aboutMeSlice from "./slices/aboutMeSlice";
import faqSlice from "./slices/faqSlice";
import initialSlice from "./slices/initialSlice";
import popupSlice from "./slices/popupSlice";
import projectFlowSlice from "./slices/projectFlowSlice";
import projectSlice from "./slices/projectSlice";
import recommendationSlice from "./slices/recommendationSlice";
import serviceSlice from "./slices/serviceSlice";
import skillSlice from "./slices/skillSlice";

const makeStore = () =>
  configureStore({
    reducer: {
      initials: initialSlice.reducer,
      projects: projectSlice.reducer,
      recommendations: recommendationSlice.reducer,
      services: serviceSlice.reducer,
      skills: skillSlice.reducer,
      faq: faqSlice.reducer,
      projectFlow: projectFlowSlice.reducer,
      aboutMe: aboutMeSlice.reducer,
      popup: popupSlice.reducer,
    },
    devtools: true,
  });

export const wrapper = createWrapper(makeStore,{debug: true})