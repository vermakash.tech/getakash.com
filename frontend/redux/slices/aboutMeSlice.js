import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchData } from "../actions/aboutMeActions";

const aboutMeSlice = createSlice({
  name: "aboutMe",
  initialState: {
    data: {
      education: [],
      experiences: [],
      certifications: [],
    },
    isSuccess: false,
    message: "",
    loading: false,
  },
  reducers: {},
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.aboutMe,
      };
    },
    //fetch education, experiences and certifications
    [fetchData.pending]: (state, { payload }) => {
      state.loading = true;
    },
    [fetchData.fulfilled]: (state, { payload }) => {
      state.data = { ...state.data, ...payload };
      state.isSuccess = true;
      state.loading = false;
    },
    [fetchData.rejected]: (state, { payload }) => {
      state.isSuccess = false;
      state.loading = false;
    },
  },
});

export default aboutMeSlice;
