import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { fetchRecommendations } from '../actions/recommendationActions';

const recommendationSlice = createSlice({
  name: 'recommendations',
  initialState: {
    data: [],
    isSuccess: false,
    message: '',
    loading: false 
  },
  reducers: {},
  extraReducers: {
    [ HYDRATE ]: (state, action) => {
      return {
              ...state,
              ...action.payload.recommendations
          };
    },
    //fetch description and tagline
    [ fetchRecommendations.pending ]: (state, {payload}) => {
        state.loading = true
    },
    [ fetchRecommendations.fulfilled ]: (state, {payload}) => {
        state.data = [...state.data, ...payload]
        state.isSuccess = true
        state.loading = false
    },
    [ fetchRecommendations.rejected ]: (state, {payload}) => {
        state.isSuccess = false
        state.loading = false
    },

  },
})

export default recommendationSlice