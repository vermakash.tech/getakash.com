import { createSlice } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

const closePopup = createAsyncThunk("popup/closePopup", async (arg, { rejectWithValue }) => {
  try {
    return {
      isPopupOpen: false,
    };
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});
const openPopup = createAsyncThunk("popup/openPopup", async (arg, { rejectWithValue }) => {
  try {
    return {
      isPopupOpen: true,
    };
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});
const setPopupData = createAsyncThunk("popup/setPopupData", async (arg, { rejectWithValue }) => {
  try {
    return {
      msg: arg.msg,
      type: arg.type,
    };
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});

const popupSlice = createSlice({
  name: "popup",
  initialState: {
    data: {
      isPopupOpen: false,
      popupMessage: "",
      popupType: "",
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.popup,
      };
    },
    //fetch all faq
    [closePopup.fulfilled]: (state, { payload }) => {
      state.data.isPopupOpen = payload.isPopupOpen;
    },
    [openPopup.fulfilled]: (state, { payload }) => {
      state.data.isPopupOpen = payload.isPopupOpen;
    },
    [setPopupData.fulfilled]: (state, { payload }) => {
      state.data.popupMessage = payload.msg;
      state.data.popupType = payload.type;
    },
  },
});

export { closePopup, openPopup, setPopupData };
export default popupSlice;
