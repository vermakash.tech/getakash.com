import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
// import { fetchFaq } from "../actions/faqActions";

const projectFlowSlice = createSlice({
  name: "projectFlow",
  initialState: {
    data: {
      isProjectFlow: false,
    },
  },
  reducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.projectFlow,
      };
    },
    toggleIsProjectFlow(state) {
      state.data.isProjectFlow = !state.data.isProjectFlow;
    },
  },
});

export const { toggleIsProjectFlow } = projectFlowSlice.actions;
export default projectFlowSlice;
