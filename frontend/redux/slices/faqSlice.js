import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { fetchFaq } from "../actions/faqActions";

const faqSlice = createSlice({
  name: "faq",
  initialState: {
    data: [],
    isSuccess: false,
    message: "",
    loading: false,
  },
  reducers: {},
  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.faq,
      };
    },
    //fetch all faq
    [fetchFaq.pending]: (state, { payload }) => {
      state.loading = true;
    },
    [fetchFaq.fulfilled]: (state, { payload }) => {
      state.data = [...state.data, ...payload];
      state.isSuccess = true;
      state.loading = false;
    },
    [fetchFaq.rejected]: (state, { payload }) => {
      state.isSuccess = false;
      state.loading = false;
    },
  },
});

export default faqSlice;
