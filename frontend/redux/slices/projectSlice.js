import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { fetchProjects } from '../actions/projectActions';

const projectSlice = createSlice({
  name: 'projects',
  initialState: {
    data: [],
    isSuccess: false,
    message: '',
    loading: false 
  },
  reducers: {},
  extraReducers: {
    [ HYDRATE ]: (state, action) => {
      return {
              ...state,
              ...action.payload.projects
          };
    },
    //fetch description and tagline
    [ fetchProjects.pending ]: (state, {payload}) => {
        state.loading = true
    },
    [ fetchProjects.fulfilled ]: (state, {payload}) => {
        state.data = [...state.data, ...payload]
        state.isSuccess = true
        state.loading = false
    },
    [ fetchProjects.rejected ]: (state, {payload}) => {
        state.isSuccess = false
        state.loading = false
    },

  },
})

export default projectSlice