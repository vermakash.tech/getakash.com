import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { fetchSkills } from '../actions/skillActions';

const skillSlice = createSlice({
  name: 'skills',
  initialState: {
    data: [],
    isSuccess: false,
    message: '',
    loading: false 
  },
  reducers: {},
  extraReducers: {
    [ HYDRATE ]: (state, action) => {
      return {
              ...state,
              ...action.payload.skills
          };
    },
    //fetch description and tagline
    [ fetchSkills.pending ]: (state, {payload}) => {
        state.loading = true
    },
    [ fetchSkills.fulfilled ]: (state, {payload}) => {
        state.data = [...state.data, ...payload]
        state.isSuccess = true
        state.loading = false
    },
    [ fetchSkills.rejected ]: (state, {payload}) => {
        state.isSuccess = false
        state.loading = false
    },

  },
})

export default skillSlice