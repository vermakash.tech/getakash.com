import { createSlice } from '@reduxjs/toolkit'
import { fetchInitials } from '../actions/initialActions'
import { HYDRATE } from 'next-redux-wrapper'

const initialSlice = createSlice({
  name: 'initials',
  initialState: {
    data: {},
    isSuccess: false,
    message: '',
    loading: false 
  },
  reducers: {},
  extraReducers: {
    [ HYDRATE ]: (state, action) => {
      return {
              ...state,
              ...action.payload.initials
          };
    },
    //fetch description and tagline
    [ fetchInitials.pending ]: (state, {payload}) => {
        state.loading = true
    },
    [ fetchInitials.fulfilled ]: (state, {payload}) => {
        state.data = {...state.data, ...payload}
        state.isSuccess = true
        state.loading = false
    },
    [ fetchInitials.rejected ]: (state, {payload}) => {
        state.isSuccess = false
        state.loading = false
    },

    // //delete task actions
    // [ deleteTask.fulfilled ]: (state, {payload}) => {
    //     state.data = state.data.filter((x)=>{
    //       return x.id !== payload
    //     })
    //     state.isSuccess = true
    //     state.loading = false
    // },

    // //add task actions
    // [ addTask.fulfilled ]: (state, {payload}) => {
    //     state.data = [...state.data, payload]
    //     state.isSuccess = true
    //     state.loading = false
    // },
  },
})

export default initialSlice