import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { fetchServices } from '../actions/serviceAction';

const serviceSlice = createSlice({
  name: 'services',
  initialState: {
    data: [],
    isSuccess: false,
    message: '',
    loading: false 
  },
  reducers: {},
  extraReducers: {
    [ HYDRATE ]: (state, action) => {
      return {
              ...state,
              ...action.payload.services
          };
    },
    //fetch description and tagline
    [ fetchServices.pending ]: (state, {payload}) => {
        state.loading = true
    },
    [ fetchServices.fulfilled ]: (state, {payload}) => {
        state.data = [...state.data, ...payload]
        state.isSuccess = true
        state.loading = false
    },
    [ fetchServices.rejected ]: (state, {payload}) => {
        state.isSuccess = false
        state.loading = false
    },

  },
})

export default serviceSlice