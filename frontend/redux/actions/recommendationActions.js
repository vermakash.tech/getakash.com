import { createAsyncThunk} from '@reduxjs/toolkit'
import recommendationsData from '../../../backend/data/recommendations'
import axios from "axios";

const fetchRecommendations = createAsyncThunk(
  "recommendations/fetchRecommendations",
  async (arg, { rejectWithValue }) => {
    try {
      console.log("fetching recommendations from database...");
      const fetchedRecomms = await axios.get("http://127.0.0.1:5000/api/recommendations");
      const recommendations = fetchedRecomms.data.data.recommendations;
      return recommendations.filter((x) => x.pinned === true);
    } catch (error) {
      rejectWithValue(error.response.data);
    }
  }
);

export {fetchRecommendations}