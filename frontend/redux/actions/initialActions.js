import { createAsyncThunk} from '@reduxjs/toolkit'
// import descriptions from '../../../backend/data/descriptions'
// import taglines from '../../../backend/data/taglines'
import axios from 'axios'

const fetchInitials = createAsyncThunk('initials/fetchInitials',async (arg,{rejectWithValue}) => {
    try{
        console.log('fetching initials data from database...')
        const fetchedDescriptions = await axios.get('http://127.0.0.1:5000/api/initials/descriptions')
        const fetchedTaglines = await axios.get('http://127.0.0.1:5000/api/initials/taglines')
        const descriptions = fetchedDescriptions.data.data.descriptions
        const taglines = fetchedTaglines.data.data.taglines;
        const description = descriptions.filter(x => x.page===arg.page)
        const tagline = taglines.filter(x => x.page===arg.page)
        return {
            description: description[0],
            tagline: tagline[0]
        }
    }
    catch(error){
        rejectWithValue(error.response.data)
    }
  }
)

export {fetchInitials}