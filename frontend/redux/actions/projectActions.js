import { createAsyncThunk} from '@reduxjs/toolkit'
import projectsData from '../../../backend/data/projects'
import axios from "axios";

const fetchProjects = createAsyncThunk("projects/fetchProjects", async (arg, { rejectWithValue }) => {
  try {
    console.log("fetching projects from database...");
    const fetchedProjects = await axios.get("http://127.0.0.1:5000/api/projects");
    const projects = fetchedProjects.data.data.projects;
    return projects.sort((a,b)=>{
      let rank1 = a.rank ? a.rank : 6
      let rank2 = b.rank ? b.rank : 6
      return rank1-rank2
    });
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});

export {fetchProjects}