import { createAsyncThunk} from '@reduxjs/toolkit'
import skillsData from '../../../backend/data/skills'
import axios from "axios";

const fetchSkills = createAsyncThunk("skills/fetchSkills", async (arg, { rejectWithValue }) => {
  try {
    console.log("fetching skills from database...");
    const fetchedSkills = await axios.get("http://127.0.0.1:5000/api/skills");
    const skills = fetchedSkills.data.data.skills.filter((x) => x.pinned);
    return skills;
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});

export {fetchSkills}