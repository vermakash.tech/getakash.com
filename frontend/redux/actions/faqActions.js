import { createAsyncThunk } from "@reduxjs/toolkit";
import faqData from "../../../backend/data/faq";
import axios from "axios";

const fetchFaq = createAsyncThunk("faq/fetchFaq", async (arg, { rejectWithValue }) => {
  try {
    console.log("fetching faq from database...");
    const fetchedFaq = await axios.get("http://127.0.0.1:5000/api/faq");
    const faq = fetchedFaq.data.data.faq;
    return faq;
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});

export { fetchFaq };
