import { createAsyncThunk} from '@reduxjs/toolkit'
import servicesData from '../../../backend/data/services'
import axios from "axios";

const fetchServices = createAsyncThunk("services/fetchServices", async (arg, { rejectWithValue }) => {
  try {
    console.log("fetching services from database...");
    const fetchedServices = await axios.get("http://127.0.0.1:5000/api/services");
    const services = fetchedServices.data.data.services;
    return services;
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});

export {fetchServices}