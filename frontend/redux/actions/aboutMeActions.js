import { createAsyncThunk } from "@reduxjs/toolkit";
import educationData from "../../../backend/data/education";
import experiencesData from "../../../backend/data/experiences";
import certificationsData from "../../../backend/data/certifications";
import axios from "axios";

const fetchData = createAsyncThunk("aboutMe/fetchData", async (arg, { rejectWithValue }) => {
  try {
    console.log("fetching about me data from database...");
    const fetchedEducation = await axios.get("http://127.0.0.1:5000/api/education");
    const fetchedExperiences = await axios.get("http://127.0.0.1:5000/api/experiences");
    const fetchedCertifications = await axios.get("http://127.0.0.1:5000/api/certifications");

    const education = fetchedEducation.data.data.education;
    const experiences = fetchedExperiences.data.data.experiences;
    const certifications = fetchedCertifications.data.data.certis;

    return {
      education: education,
      experiences: experiences.sort((a,b)=>a.rank-b.rank),
      certifications: certifications,
    };
  } catch (error) {
    rejectWithValue(error.response.data);
  }
});

export { fetchData };
