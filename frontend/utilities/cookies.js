const setCookie = (cname, cvalue, maxAge, attrString) => {
  let cookie = `${cname}=${cvalue}; max-age=${maxAge}; ${attrString}`;
  document.cookie = cookie;
};

const getCookie = (cname, cvalue) => {
  let desiredCookie = document.cookie.split(";").filter((x) => x.includes(cname));
  if (desiredCookie.length > 0) {
    let cookieValue = desiredCookie[0].split("=");
    return cookieValue;
  }
  return null;
};

export { setCookie, getCookie };
