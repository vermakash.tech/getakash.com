import { createGlobalStyle } from 'styled-components'
import {font, color, bp} from './variables'

const GlobalStyle = createGlobalStyle`
    
    *,
    *::before,
    *::after {
        margin: 0;
        padding: 0;
        box-sizing: inherit;
    }

    html {
        box-sizing: border-box;
        font-size: 62.5%; // 10px/16px = 62.5% -> 1rem = 10px
        font-family: ${font.primary};

        @media only screen and (max-width: ${bp.largest}) {
            font-size: 50%; // 8px/16px = 50% -> 1rem = 8px
        }
    }

    body {
        font-family: ${font.primary};
        color: ${color.grey_dark_2};
        font-weight: 300;
        line-height: 1.6;

        -ms-overflow-style: none;  /* IE and Edge */
        scrollbar-width: none;  /* Firefox */
        &::-webkit-scrollbar {
            display: none;
        }
    }
`;

export default GlobalStyle