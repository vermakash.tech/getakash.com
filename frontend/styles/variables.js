// FONT VARIABLES
export const font = {
    primary : `'Lato', sans-serif`,
    heading : `'Montserrat', sans-serif`,
}

// RESPONSIVE BREAKPOINTS
export const bp = {
  x_largest: "100em", // 1600px
  largest: "75em", // 1200px
  large: "62em", // 992px
  medium: "48em", // 768px
  small: "36em", // 576px
  x_small: "28em", // 448px
  xx_small: "21em", // 336px
};

// COLOR VARIABLES
export const color = {
  primary: "#6237A0",
  primary_light: "#9754CB", //151, 84, 203
  primary_extra_light: "#DEACF5",
  primary_dark: "#28104E",

  secondary: "#01B7B7",
  secondary_light: "#47CFCF", //71, 207, 207
  secondary_extra_light: "#AFEAE6",
  secondary_dark: "#007171",

  tertiary: "#F3A700",
  tertiary_light: "#FFC23C",
  tertiary_extra_light: "#FFDE95",
  tertiary_dark: "#BA8000",

  white: "#FBFCFF",
  white_dark: "#DDDFE8",

  grey: "#5C5D63",
  grey_light: "#ADAFB9",

  black: "#1A1A1D",
  black_light: "#36383E",
};