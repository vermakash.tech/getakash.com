import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledLoader = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  /* position: relative; */
  .loader3 {
    /* position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%); */
    width: 150px;
    height: 150px;
    /* display: inline-block; */
    padding: 0px;
    /* text-align: left; */

    span {
      position: absolute;
      display: inline-block;
      width: 150px;
      height: 150px;
      border-radius: 100%;
      background: ${color.primary};
      -webkit-animation: loader3 1.5s linear infinite;
      animation: loader3 1.5s linear infinite;

      &:last-child {
        animation-delay: -0.9s;
        -webkit-animation-delay: -0.9s;
      }
    }
  }
  @keyframes loader3 {
    0% {
      transform: scale(0, 0);
      opacity: 0.8;
    }
    100% {
      transform: scale(1, 1);
      opacity: 0;
    }
  }
  @-webkit-keyframes loader3 {
    0% {
      -webkit-transform: scale(0, 0);
      opacity: 0.8;
    }
    100% {
      -webkit-transform: scale(1, 1);
      opacity: 0;
    }
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.xx_small}) {
  }
`;

export default StyledLoader;
