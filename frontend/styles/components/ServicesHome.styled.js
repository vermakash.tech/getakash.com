import styled from "styled-components";
import { font, color, bp } from "../variables";

const color_primary_light_75 = `${color.primary_light}CC`;

const StyledServicesHome = styled.div`
  margin-top: 10rem;
  margin-bottom: 51rem;
  width: 100vw;

  .servicesHome {
    &-content {
      width: 100%;
      padding-top: 9rem;
      height: 58rem;
      background-image: linear-gradient(rgba(222, 172, 245, 0.75), rgba(222, 172, 245, 0.75)),
        url("/images/servicesHomeBg.jpg");
      background-size: cover;
      background-position: center;
    }
    &-heading {
      text-align: center;
      font-family: ${font.heading};
      font-size: 5rem;
      font-weight: 700;
      letter-spacing: -1px;
      color: ${color.black};
      margin-bottom: 6rem;
      text-shadow: 0 2px 4px #9b9da3;
      &-redirect {
        cursor: pointer;
        transform: scale(0.9);
        transition: all 0.1s;
        &:hover {
          color: ${color.primary};
          transform: scale(0.95);
        }
      }
    }
    &-desc {
      text-align: justify;
      font-family: ${font.primary};
      font-size: 2.5rem;
      font-weight: 500;
      text-shadow: 0 2px 4px #9b9da3;
    }
    &-row {
      margin-top: -15rem;
    }
    &-single {
      display: flex;
      justify-content: center;
      position: relative;
    }
  }

  .servicecard {
    width: 85%;
    border-radius: 1px;
    position: absolute;
    top: 0;
    box-shadow: 0.4rem 0.8rem 1rem ${color.grey_light};
    background: linear-gradient(to bottom right, rgb(71, 207, 207), rgb(71, 207, 207), rgb(151, 84, 203));
    padding: 3px;

    &-container {
      width: 100%;
      background: linear-gradient(to bottom right, rgb(71, 207, 207), rgb(71, 207, 207), rgb(151, 84, 203));
    }
    &-img {
      width: 100%;
    }
    &-body {
      color: ${color.white};
      padding: 1.4rem;
      text-shadow: 0 2px 4px ${color.grey};
      position: relative;
      &-button {
        position: absolute;
        top: -2rem;
        padding: 1rem;
        border-radius: 50%;
        text-align: center;
        text-decoration: none;
        border: none;
        background-color: ${color.primary_light};
        display: flex;
        justify-content: center;
        align-items: center;

        &-cross {
          width: 2rem;
          color: ${color.white};
          font-size: 2rem;
        }
        &-plus {
          width: 2rem;
          color: ${color.white};
          font-size: 2rem;
        }
      }
      &-heading {
        text-align: center;
        font-size: 2.5rem;
        font-family: ${font.heading};
        font-weight: 700;
        padding: 0.5rem;
        margin: 1.5rem 0 1.5rem 0;
      }
      &-desc {
        overflow-y: hidden;
        font-family: ${font.primary};
        font-size: 1.8rem;
        font-weight: 300;
        text-align: center;
        max-height: 40rem;
        margin: 0 1rem 1rem 1rem;
        transition: all 0.5s ease;
        &-open {
          animation: roll_down 0.8s ease-out;
        }
        &-hidden {
          max-height: 0;
        }
      }
    }
  }

  @keyframes name {
  }

  @media only screen and (min-width: ${bp.x_largest}) {
    margin-bottom: 56rem;
  }
  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
    margin-bottom: 42.5rem;
    .servicesHome {
      &-content {
        height: 55rem;
      }
      &-heading {
        font-size: 4.5rem;
      }
      &-desc {
        font-size: 2.25rem;
      }
      &-row {
      }
      &-single {
        display: flex;
        justify-content: center;
        position: relative;
      }
    }
    .servicecard {
      width: 90%;

      &-container {
      }
      &-img {
      }
      &-body {
        padding: 1.2rem;

        &-button {
          &-cross {
          }
          &-plus {
          }
        }
        &-heading {
          font-size: 1.8rem;
        }
        &-desc {
          font-size: 1.4rem;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    margin-bottom: 5rem;
    .servicesHome {
      &-content {
        height: 62rem;
      }
      &-heading {
        font-size: 3.8rem;
      }
      &-desc {
        font-size: 2.1rem;
      }
      &-row {
        margin-top: -18rem;
      }
      &-single {
        margin-bottom: 3rem;
      }
    }
    .servicecard {
      width: 70%;
      border-radius: 1px;
      position: static;
      display: block;
      &-container {
      }
      &-img {
      }
      &-body {
        padding: 0.8rem;

        &-button {
          position: absolute;
          top: -3rem;
          padding: 1.5rem;

          &-cross {
            width: 3rem;
            color: ${color.white};
            font-size: 3rem;
          }
          &-plus {
            width: 3rem;
            color: ${color.white};
            font-size: 3rem;
          }
        }
        &-heading {
          font-size: 3rem;
          margin: 2.25rem 0 1rem 0;
        }
        &-desc {
          font-size: 2.25rem;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    margin-bottom: 5rem;
    .servicesHome {
      &-content {
        height: 62rem;
      }
      &-heading {
        font-size: 3.8rem;
      }
      &-desc {
        font-size: 2.1rem;
      }
      &-row {
        margin-top: -18rem;
      }
      &-single {
        margin-bottom: 3rem;
      }
    }
    .servicecard {
      width: 75%;
      border-radius: 1px;
      position: static;
      display: block;
      &-container {
      }
      &-img {
      }
      &-body {
        padding: 0.8rem;

        &-button {
          position: absolute;
          top: -3rem;
          padding: 1.5rem;

          &-cross {
            width: 3rem;
            color: ${color.white};
            font-size: 3rem;
          }
          &-plus {
            width: 3rem;
            color: ${color.white};
            font-size: 3rem;
          }
        }
        &-heading {
          font-size: 2.5rem;
          margin: 2.25rem 0 1rem 0;
        }
        &-desc {
          font-size: 2rem;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .servicesHome {
      &-heading {
        font-size: 3.5rem;
        margin-bottom: 4rem;
      }
      &-desc {
        font-size: 1.9rem;
      }
      &-row {
        margin-top: -15rem;
      }
      &-single {
        margin-bottom: 4rem;
      }
    }
    .servicecard {
      width: 90%;

      &-container {
      }
      &-img {
      }
      &-body {
        &-button {
          position: absolute;
          top: -2.75rem;
          padding: 1.25rem;
          &-cross {
            width: 2.75rem;
            color: ${color.white};
            font-size: 2.75rem;
          }
          &-plus {
            width: 2.75rem;
            color: ${color.white};
            font-size: 2.75rem;
          }
        }
        &-heading {
        }
        &-desc {
          font-size: 1.5rem;
        }
      }
    }
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .servicesHome {
      &-heading {
        font-size: 3.5rem;
        margin-bottom: 4rem;
      }
      &-desc {
        font-size: 1.9rem;
      }
      &-row {
        margin-top: -13rem;
      }
      &-single {
        margin-bottom: 4rem;
      }
    }
    .servicecard {
      width: 90%;

      &-container {
      }
      &-img {
      }
      &-body {
        &-button {
          position: absolute;
          top: -2.5rem;
          padding: 1.25rem;
          &-cross {
            width: 2.5rem;
            color: ${color.white};
            font-size: 2.5rem;
          }
          &-plus {
            width: 2.5rem;
            color: ${color.white};
            font-size: 2.5rem;
          }
        }
        &-heading {
        }
        &-desc {
        }
      }
    }
  }
`;

export default StyledServicesHome;
