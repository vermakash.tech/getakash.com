import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledProjectCard = styled.div`
  width: ${(props) => (props.is_reduced ? "10rem" : "40rem")};
  box-shadow: ${(props) =>
    props.isShadow ? `0.4rem 0.8rem 1.2rem ${color.grey_light}` : `0.4rem 0.8rem 1.2rem transparent`};
  background-color: #eed1fb;
  animation: ${(props) => props.mainAnimation} 0.2s ease-out;
  .project_card {
    &-img_container_wrapper {
      filter: drop-shadow(0 0.3rem 0.3rem ${color.grey_light});
      position: relative;
    }
    &-title {
      position: absolute;
      bottom: 0;
      right: 2rem;
      padding: 1.5rem 2.5rem;
      font-size: 3rem;
      font-weight: 700;
      font-family: ${font.heading};
      background-color: ${color.secondary_light};
      color: ${color.black};
      text-align: center;
      /* cursor: pointer; */
      animation: ${(props) => props.animation_name} 0.2s ease-out;
      transition: all 0.1s;
      &:hover {
        transform: scale(1.03);
      }
    }
    &-img_container {
      width: 100%;
      height: 25rem;
      clip-path: polygon(0 0, 100% 0, 100% 75%, 0 100%);
      box-shadow: 0.3rem 0.6rem 0.6rem ${color.grey};
      background-image: url(${(props) => props.bg_image});
      background-size: cover;
      background-position: center;
    }
    &-body {
      padding: 3.5rem 2rem 0.5rem 2rem;
      h3 {
        font-size: 1.8rem;
        font-weight: 500;
        text-align: justify;
      }
      &-links_container {
        display: flex;
        justify-content: right;
        align-items: center;
        margin: 2rem 0;
      }
      &-link {
        text-decoration: none;
        color: ${color.primary};
        font-family: ${font.heading};
        font-size: 2.25rem;
        font-weight: 700;
        &:hover {
          text-decoration: underline;
        }
        &:not(:last-child) {
          margin-right: 2rem;
        }
        &-inactive {
          cursor: not-allowed;
          opacity: 70%;
          text-decoration: none !important;
        }
      }
    }
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
    .project_card {
      &-img_container_wrapper {
      }
      &-title {
      }
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
    width: 32rem;
    .project_card {
      &-img_container_wrapper {
        filter: drop-shadow(0 0.3rem 0.3rem ${color.grey_light});
        position: relative;
      }
      &-title {
        position: absolute;
        bottom: 0;
        right: 2rem;
        padding: 1rem 2rem;
        font-size: 2.5rem;
      }
      &-body {
        padding: 3.5rem 2rem 0.5rem 2rem;
        h3 {
          font-size: 1.75rem;
          font-weight: 500;
          text-align: justify;
        }
      }
    }
  }
`;

export default StyledProjectCard;
