import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledSiteLink = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  .site_link {
    &-curve {
    }
    &-link {
      font-size: 2.75rem;
      margin-left: 1rem;
      background-image: linear-gradient(to right bottom, ${color.primary_light}, ${color.primary_dark});
      -webkit-background-clip: text;
      background-clip: text;
      color: transparent;
      font-weight: 700;
      letter-spacing: 1.1;
      cursor: pointer;
      transition: all 0.1s;
      :hover {
        text-decoration: underline;
        text-decoration-color: ${color.primary};
      }
    }
  }
  .wavy-line {
    width: 15rem;
    height: 5rem;
    overflow: hidden;
    /* margin: 0 auto 0 auto; */

    &::before {
      content: attr(data-text);
      position: relative;
      top: -2.7rem;
      color: rgba(0, 0, 0, 0);
      width: calc(100% + 2.7rem);
      font-size: 4rem;
      text-decoration-style: wavy;
      text-decoration-color: #25173a;
      text-decoration-line: underline;
      /* animation: animate 0.9s linear infinite;
      -webkit-animation: animate 0.9s linear infinite; */
    }
  }
  .wavy-line-green::before {
    color: transparent;
    text-decoration-color: ${color.primary_light};
  }
  @keyframes animate {
    0% {
      left: -0px;
    }
    100% {
      left: -4rem;
    }
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
  }
  @media only screen and (max-width: ${bp.xx_small}) {
  }
`;

export default StyledSiteLink;
