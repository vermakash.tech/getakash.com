import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledFaq = styled.div`
  margin-top: 15rem;
  .faq {
    &-heading {
      width: 100%;
      text-align: center;
      font-family: ${font.heading};
      font-size: 5rem;
      font-weight: 700;
      letter-spacing: -1px;
      color: ${color.black};
      margin-bottom: 6rem;
      text-shadow: 0 1px 2px #9b9da3;
    }
    &-row {
    }
    &-col {
      margin: 0 auto;
    }
  }
  .faq_card {
    transition: transform 0.8s;
    margin-bottom: 2rem;
    height: 12rem;
    position: relative;
    &-side {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      border-radius: 1rem;
      color: ${color.white};
      text-align: center;
      backface-visibility: hidden;
      transition: all 0.4s ease-out;
      box-shadow: 0.2rem 0.6rem 1rem ${color.grey_light};
      &-front {
        background-image: linear-gradient(to right, ${color.primary}, ${color.primary_light});
        padding: 0 3rem;
        font-size: 2.25rem;
        font-weight: 500;
        overflow: hidden;
      }
      &-back {
        background-image: linear-gradient(to right, ${color.secondary_dark}, ${color.secondary});
        font-size: 1.75rem;
        line-height: 1.2;
        font-weight: 300;
        padding: 0 2rem;
        text-align: center;
        transform: rotateX(-180deg);
        opacity: 0;
        visibility: hidden;
      }
    }
    &:hover ${".faq_card-side-front"} {
      transform: rotateX(180deg) scale(0.95);
    }
    &:hover ${".faq_card-side-back"} {
      transform: rotateX(0) scale(0.95);
      opacity: 1;
      visibility: visible;
    }
  }
  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .faq_card {
      &-front {
        padding: 0 3rem;
        font-size: 2rem;
      }
      &-back {
        padding: 0 2rem;
        font-size: 1.6rem;
      }
    }
  }
`;

export default StyledFaq;
