import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledProjectRequest = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: ${`${color.primary}E6`};
  backdrop-filter: blur(2px);
  z-index: 5000;
  .project_request {
    &-container {
      width: 50%;
      height: 45rem;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      padding: 6rem 6rem 5rem 6rem;
      color: ${color.white};
      background-image: linear-gradient(to right bottom, ${color.primary_light}, ${color.secondary});
      border-radius: 2px;
      animation: pop_out_certi 0.2s ease-out;
    }
    &-cross {
      font-size: 2.5rem;
      position: absolute;
      top: 1rem;
      right: 1.25rem;
      color: ${color.black_light};
      cursor: pointer;
      transition: all 0.1s;
      &:hover {
        transform: scale(1.05);
      }
    }
    &-hey {
      font-size: 4.5rem;
      margin-bottom: 4.5rem;
      font-weight: 700;
    }
    &-chunk {
      &-label {
        display: block;
        font-size: 2.4rem;
        margin-bottom: 1.25rem;
        line-height: 1;
        &-single {
          margin-top: 3.5rem;
        }
      }
      &-select {
        background-color: ${color.white};
        font-size: 1.9rem;
        padding: 0.5rem 1rem;
        border: none;
        outline: none;
        border-radius: 2px;
        margin-bottom: 4rem;
        width: 27.5rem;
      }
      &-textarea {
        background-color: ${color.white};
        font-size: 2rem;
        padding: 0.5rem 1rem;
        border: none;
        outline: none;
        border-radius: 2px;
        margin-bottom: 4rem;
        width: 51rem;
        height: 12.5rem;
      }
      &-actions {
        display: flex;
        justify-content: space-between;
        margin: 0 4rem;
        position: absolute;
        bottom: 3.5rem;
        left: 0;
        right: 0;
        &-next {
          font-size: 2rem;
          cursor: pointer;
          transition: all 0.2s;
          &:hover {
            text-decoration: underline;
          }
          &-icon {
            margin-left: 1rem;
          }
        }
        &-back {
          font-size: 2rem;
          cursor: pointer;
          transition: all 0.1s;
          &:hover {
            text-decoration: underline;
          }
          &-icon {
            margin-right: 1rem;
          }
        }
        &-inactive {
          opacity: 0.5;
          cursor: not-allowed;
          &:hover {
            text-decoration: none;
          }
        }
      }
    }
    &-chunk_success {
      margin-top: 8rem;
      svg {
        width: 10rem;
        display: block;
        margin: 4rem auto 0;
        color: ${color.white};
      }

      .path {
        stroke-dasharray: 1000;
        stroke-dashoffset: 0;
        &.circle {
          -webkit-animation: dash 0.9s ease-in-out;
          animation: dash 0.9s ease-in-out;
        }
        &.line {
          stroke-dashoffset: 1000;
          -webkit-animation: dash 0.9s 0.35s ease-in-out forwards;
          animation: dash 0.9s 0.35s ease-in-out forwards;
        }
        &.check {
          stroke-dashoffset: -100;
          -webkit-animation: dash-check 0.9s 0.35s ease-in-out forwards;
          animation: dash-check 0.9s 0.35s ease-in-out forwards;
        }
      }

      p {
        text-align: center;
        margin: 2rem 0 6rem;
        font-size: 2rem;
        &.success {
          color: ${color.white};
        }
      }
    }
  }

  @-webkit-keyframes dash {
    0% {
      stroke-dashoffset: 1000;
    }
    100% {
      stroke-dashoffset: 0;
    }
  }

  @keyframes dash {
    0% {
      stroke-dashoffset: 1000;
    }
    100% {
      stroke-dashoffset: 0;
    }
  }

  @-webkit-keyframes dash-check {
    0% {
      stroke-dashoffset: -100;
    }
    100% {
      stroke-dashoffset: 900;
    }
  }

  @keyframes dash-check {
    0% {
      stroke-dashoffset: -100;
    }
    100% {
      stroke-dashoffset: 900;
    }
  }

  @media only screen and (max-width: ${bp.largest}) {
    .project_request {
      &-container {
        width: 60%;
        height: 50rem;
      }
      &-chunk {
        &-label {
          font-size: 2.8rem;
          /* margin-bottom: 1.25rem; */
          /* line-height: 1; */
          &-single {
            /* margin-top: 3.5rem; */
          }
        }
        &-select {
          font-size: 2.2rem;
          /* padding: 0.5rem 1rem; */
          margin-bottom: 5rem;
          /* width: 25rem; */
        }
        &-textarea {
          font-size: 2.2rem;
          /* padding: 0.5rem 1rem; */
          /* margin-bottom: 4rem; */
          width: 65rem;
          height: 15rem;
        }
        &-actions {
          /* margin: 0 4rem; */
          /* bottom: 3.5rem; */
          &-next {
            font-size: 2.2rem;
            &-icon {
              /* margin-left: 1rem; */
            }
          }
          &-back {
            font-size: 2.2rem;
            &-icon {
              /* margin-right: 1rem; */
            }
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.large}) {
    .project_request {
      &-container {
        width: 70%;
        height: 50rem;
      }
      &-chunk {
        &-label {
          /* font-size: 2.4rem; */
          /* margin-bottom: 1.25rem; */
          /* line-height: 1; */
          &-single {
            /* margin-top: 3.5rem; */
          }
        }
        &-select {
          /* font-size: 1.9rem; */
          /* padding: 0.5rem 1rem; */
          /* margin-bottom: 4rem; */
          /* width: 25rem; */
        }
        &-textarea {
          /* font-size: 2rem; */
          /* padding: 0.5rem 1rem; */
          /* margin-bottom: 4rem; */
          width: 55rem;
          height: 15rem;
        }
        &-actions {
          /* margin: 0 4rem; */
          /* bottom: 3.5rem; */
          &-next {
            /* font-size: 2rem; */
            &-icon {
              /* margin-left: 1rem; */
            }
          }
          &-back {
            /* font-size: 2rem; */
            &-icon {
              /* margin-right: 1rem; */
            }
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .project_request {
      &-container {
        width: 80%;
        height: 47.5rem;
      }
      &-chunk {
        &-label {
          &-single {
          }
        }
        &-select {
        }
        &-textarea {
          width: 47.5rem;
          height: 15rem;
        }
        &-actions {
          &-next {
            &-icon {
            }
          }
          &-back {
            &-icon {
            }
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .project_request {
      &-container {
        width: 85%;
        height: 50rem;
        padding: 6rem 3rem 5rem 3rem;
      }
      &-chunk {
        &-label {
          margin-bottom: 1rem;
          /* font-size: 2.2rem; */
          line-height: 1.1;
          &-single {
          }
        }
        &-select {
          /* font-size: 1.7rem; */
          margin-bottom: 4rem;
        }
        &-textarea {
          font-size: 1.8rem;
          width: 37.5rem;
          height: 12.5rem;
        }
        &-actions {
          &-next {
            font-size: 2.2rem;
            &-icon {
            }
          }
          &-back {
            font-size: 2.2rem;
            &-icon {
            }
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .project_request {
      &-container {
        width: 90%;
      }
      &-chunk {
        &-label {
          &-single {
          }
        }
        &-select {
        }
        &-textarea {
          width: 30rem;
          height: 17.5rem;
        }
        &-actions {
          margin: 0 2.5rem;
          &-next {
            &-icon {
              margin-left: 0.5rem;
            }
          }
          &-back {
            &-icon {
              margin-right: 0.5rem;
            }
          }
        }
      }
    }
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .project_request {
      &-container {
        width: 90%;
      }
      &-chunk {
        &-label {
          font-size: 2.4rem;
          &-single {
          }
        }
        &-select {
          font-size: 1.7rem;
          margin-bottom: 3rem;
          width: 25rem;
        }
        &-textarea {
          font-size: 1.8rem;
          width: 25rem;
          height: 15rem;
        }
        &-actions {
          &-next {
            &-icon {
            }
          }
          &-back {
            &-icon {
            }
          }
        }
      }
    }
  }
`;

export default StyledProjectRequest;
