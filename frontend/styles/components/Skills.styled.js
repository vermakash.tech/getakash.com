import styled from 'styled-components'
import {font, color, bp} from '../variables'

const StyledSkills = styled.div`
  background-color: ${(props) => (props.fillBackground ? color.white_dark : "auto")};
  .skillCard {
    /* background-color: ${color.primary}; */
    display: inline-block;
    padding: 4rem;
    clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);
    margin-top: 7rem;

    img {
      width: 8rem;
      height: 8rem;
      transition: all 0.2s;

      &:hover {
        transform: scale(1.05);
      }
    }
  }
  .skills {
    margin-top: 3rem;
    margin-bottom: 3rem;
    padding-bottom: ${(props) => (props.fillBackground ? "7rem" : "0")};
    &-single {
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  #skill-0,
  #skill-4 {
    background-color: ${color.primary_light};
  }
  #skill-1,
  #skill-5 {
    background-color: ${color.primary};
  }
  #skill-2,
  #skill-6 {
    background-color: ${color.primary};
  }
  #skill-3,
  #skill-7 {
    background-color: ${color.primary_dark};
  }

  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
    .skillCard {
      background-color: ${color.primary};
      display: inline-block;
      padding: 3.5rem;
      clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);
      margin-top: 5rem;

      img {
        width: 7rem;
        height: 7rem;
      }
    }
    .skills {
      padding-bottom: ${(props) => (props.fillBackground ? "5rem" : "0")};
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    #skill-0,
    #skill-1 {
      background-color: ${color.primary_light};
    }
    #skill-2,
    #skill-3 {
      background-color: ${color.primary};
    }
    #skill-4,
    #skill-5 {
      background-color: ${color.primary};
    }
    #skill-6,
    #skill-7 {
      background-color: ${color.primary_dark};
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
  }
`;

export default StyledSkills