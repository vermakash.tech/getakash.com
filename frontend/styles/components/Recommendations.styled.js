import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledRecommendations = styled.div`
  .recommendations {
    &-heading {
      text-align: center;
      font-family: ${font.heading};
      font-size: 5rem;
      font-weight: 700;
      letter-spacing: -1px;
      color: ${color.black};
      /* margin-bottom: 2rem; */
      text-shadow: 0 2px 4px #9b9da3;
    }
    &-paginate_container {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    &-paginate {
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 1.5rem;
      /* background-image: linear-gradient(
        to right bottom,
        ${color.primary_light},
        ${color.primary},
        ${color.primary_light}
      ); */
      width: 50%;
      border-radius: 5rem;
      &-box {
        width: 2.5rem;
        height: 2.5rem;
        border-radius: 50%;
        background-color: ${color.primary_extra_light};
        cursor: pointer;
        &:not(:last-child) {
          margin-right: 1rem;
        }
        &-active {
          background-color: ${color.primary};
        }
      }
    }
  }
  .recommendation_card {
    background-image: url("recomm_huge_1.svg");
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    height: 60rem;
    display: flex;
    align-items: center;
    justify-content: center;
    animation: pop_out 0.2s ease-out;
    &-container {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      width: 45%;
    }
    &-user {
      color: ${color.primary};
      font-size: 2.6rem;
      margin-left: 0.5rem;
      transition: all 0.1s;
    }
    &-desc {
      background-image: linear-gradient(to right bottom, ${color.primary_dark}, ${color.primary_light});
      -webkit-background-clip: text;
      background-clip: text;
      color: transparent;
      text-align: center;
      font-size: 2rem;
      font-weight: 700;
      margin-bottom: 3.5rem;
    }
    &-name {
      background-image: linear-gradient(to right bottom, ${color.primary_light}, ${color.black});
      -webkit-background-clip: text;
      background-clip: text;
      color: transparent;
      margin-bottom: 0;
      letter-spacing: 0.8;
      /* color: ${color.primary}; */
      font-family: ${font.heading};
      align-self: flex-end;
      font-size: 2.5rem;
      display: flex;
      align-items: center;
      font-weight: 700;
      transition: all 0.2s;
      cursor: pointer;
      &:hover {
        text-decoration: underline;
        text-decoration-color: ${color.primary};
        .recommendation_card-user {
          transform: scale(1.1);
        }
      }
    }
    &-profession {
      background-image: linear-gradient(to right bottom, ${color.primary_light}, ${color.black});
      -webkit-background-clip: text;
      background-clip: text;
      color: transparent;
      letter-spacing: 0.8;
      font-family: ${font.heading};
      /* color: ${color.primary}; */
      align-self: flex-end;
      font-size: 2rem;
      font-weight: 500;
      span {
      }
    }
  }

  @keyframes name {
  }

  @media only screen and (min-width: ${bp.x_largest}) {
    .recommendation_card {
      &-container {
        width: 37.5%;
      }
    }
  }

  @media only screen and (max-width: ${bp.largest}) {
    .recommendation_card {
      &-container {
        width: 47.5%;
      }
    }
  }

  @media only screen and (max-width: ${bp.large}) {
    .recommendation_card {
      height: 47.5rem;
      &-container {
      }
      &-user {
        font-size: 2.6rem;
        margin-left: 0.5rem;
      }
      &-desc {
        text-align: center;
        font-size: 1.9rem;
        font-weight: 700;
        margin-bottom: 1.5rem;
      }
      &-name {
        letter-spacing: 0.8;
        font-size: 2.3rem;
        font-weight: 700;
      }
      &-profession {
        font-size: 1.9rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .recommendations {
      &-heading {
        font-size: 4rem;
      }
    }
    .recommendation_card {
      height: 40rem;
      &-container {
        width: 47.5%;
      }
      &-user {
        font-size: 2rem;
        margin-left: 0.5rem;
      }
      &-desc {
        line-height: 0.99;
        text-align: center;
        font-size: 1.6rem;
        font-weight: 700;
        margin-bottom: 1.5rem;
      }
      &-name {
        text-decoration: underline;
        letter-spacing: 0.8;
        font-size: 2rem;
        font-weight: 700;
      }
      &-profession {
        font-size: 1.7rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .recommendation_card {
      height: 37.5rem;
      &-container {
        width: 55%;
      }
      &-user {
        font-size: 2rem;
        margin-left: 0.5rem;
      }
      &-desc {
        line-height: 0.99;
        text-align: center;
        font-size: 1.5rem;
        font-weight: 700;
        margin-bottom: 1rem;
      }
      &-name {
        text-decoration: underline;
        letter-spacing: 0.8;
        font-size: 1.75rem;
        font-weight: 700;
      }
      &-profession {
        font-size: 1.5rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .recommendations {
      &-heading {
        font-size: 3.5rem;
      }
    }
    .recommendation_card {
      height: 32.5rem;
      &-container {
        width: 52.5%;
      }
      &-user {
        font-size: 1.8rem;
        margin-left: 0.5rem;
      }
      &-desc {
        line-height: 0.99;
        text-align: center;
        font-size: 1.2rem;
        font-weight: 700;
        margin-bottom: 1rem;
      }
      &-name {
        text-decoration: underline;
        letter-spacing: 0.8;
        font-size: 1.5rem;
        font-weight: 700;
      }
      &-profession {
        font-size: 1.3rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
    .recommendations {
      &-heading {
        font-size: 3rem;
      }
    }
    .recommendation_card {
      height: 27.5rem;
      &-container {
        width: 52.5%;
      }
      &-user {
        font-size: 1.8rem;
        margin-left: 0.5rem;
      }
      &-desc {
        line-height: 0.99;
        text-align: center;
        font-size: 1.1rem;
        font-weight: 700;
        margin-bottom: 1rem;
      }
      &-name {
        text-decoration: underline;
        letter-spacing: 0.8;
        font-size: 1.3rem;
        font-weight: 700;
      }
      &-profession {
        font-size: 1.1rem;
      }
    }
  }
`;

export default StyledRecommendations;
