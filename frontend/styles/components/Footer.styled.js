import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledFooter = styled.div`
  margin-top: 25rem;
  width: 100vw;
  .footer {
    position: relative;
    background-color: ${color.primary_dark};
    padding-top: 13rem;
    &-row {
    }
    &-column {
      margin: 0 auto;
      &-left {
        display: flex;
        align-items: center;
        justify-content: center;

        &-logo {
          height: 12rem;
        }
      }
      &-right {
        &-row {
          padding: 4rem 0;
          div {
            a {
              text-decoration: none;
              h2 {
                margin-bottom: 2rem;
              }
            }
          }
        }
        &-link_container {
        }
        &-link {
          font-size: 2.5rem;
          color: ${color.white};
          text-decoration: none;
          transition: all 0.1s;
          &:not(:last-child) {
            margin-bottom: 2rem;
          }

          &:hover {
            text-decoration: underline;
            color: ${color.white};
          }
        }
      }
    }
    &-mini {
      padding: 3rem 0;
      background-color: #1b0938;
      &-container {
        display: flex;
        justify-content: space-between;
      }
      &-link_container {
        display: flex;
      }
      &-link {
        padding: 1.5rem;
        border-radius: 50%;
        background-color: ${color.primary_dark};
        color: ${color.primary_extra_light};
        box-shadow: 2px 4px 8px ${color.black};
        font-size: 2.5rem;
        margin-right: 2.5rem;
        cursor: pointer;
        transition: all 0.1s;

        &:hover {
          transform: scale(1.05);
        }
      }
      &-copyright {
        font-size: 2rem;
        letter-spacing: 2px;
        color: ${color.white};
        font-weight: 300;
        display: flex;
        align-items: center;
      }
    }
    &-contact {
      width: 90%;
      position: absolute;
      top: -10rem;
      left: 50%;
      transform: translate(-50%, 0);
      padding: 4rem;
      border-radius: 5px;
      background-image: linear-gradient(
        to right bottom,
        ${color.secondary},
        ${color.primary_light},
        ${color.secondary_light}
      );

      &-form {
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: space-evenly;
      }
      &-form_container {
        height: 100%;
        :not(:last-child) {
          margin-right: 2rem;
        }

        &-md {
          display: flex;
          align-items: center;
        }
      }
      &-input {
        border-radius: 3px;
        display: block;
        width: 28rem;
        outline: none;
        border: none;
        color: ${color.black};
        padding: 0.5rem 1rem;
        font-size: 2.1rem;
        &::placeholder {
          color: ${color.grey_light};
        }
        :not(:last-child) {
          margin-bottom: 2rem;
        }
        &:focus {
          outline: 3px solid ${color.secondary};
          outline-offset: 1px;
          border: none;
        }
      }
      &-textarea {
        border-radius: 3px;
        outline: none;
        border: none;
        width: 56rem;
        height: 11rem;
        color: ${color.black};
        padding: 0.7rem 1.4rem;
        font-size: 2.1rem;
        &::placeholder {
          color: ${color.grey_light};
        }

        &:focus {
          outline: 3px solid ${color.secondary};
          outline-offset: 1px;
          border: none;
        }
      }
      &-button {
        background-color: transparent;
        text-decoration: none;
        border: none;
        transition: all 0.1s;
        margin-right: 1rem;

        &:hover {
          transform: scale(1.05);
        }
      }
      &-arrow_right {
        color: ${color.primary};
        font-size: 9rem;
        text-shadow: 3px 5px 10px ${color.grey};
        transform: scaleX(1.1);
      }
    }
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
    .footer {
      padding-top: 12rem;
      &-row {
      }
      &-column {
        &-left {
          &-logo {
          }
        }
        &-right {
          &-row {
          }
          &-link_container {
          }
          &-link {
            &:not(:last-child) {
            }

            &:hover {
            }
          }
        }
      }
      &-mini {
        &-container {
        }
        &-link_container {
          display: flex;
        }
        &-link {
          cursor: pointer;

          &:hover {
          }
        }
        &-copyright {
        }
      }
      &-contact {
        &-form {
        }
        &-form_container {
          :not(:last-child) {
          }

          &-md {
          }
        }
        &-input {
          &::placeholder {
          }
          :not(:last-child) {
          }
          &:focus {
          }
        }
        &-textarea {
          &::placeholder {
          }

          &:focus {
          }
        }
        &-button {
          &:hover {
          }
        }
        &-arrow_right {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.large}) {
    margin-top: 23rem;
    .footer {
      padding-top: 10rem;
      &-row {
      }
      &-column {
        &-left {
          &-logo {
            height: 10rem;
          }
        }
        &-right {
          &-row {
            padding-left: 2rem;
          }
          &-link_container {
          }
          &-link {
            font-size: 2.1rem;
            &:not(:last-child) {
            }

            &:hover {
            }
          }
        }
      }
      &-mini {
        &-container {
        }
        &-link_container {
          display: flex;
        }
        &-link {
          cursor: pointer;

          &:hover {
          }
        }
        &-copyright {
        }
      }
      &-contact {
        width: 95%;
        &-form {
        }
        &-form_container {
          :not(:last-child) {
          }

          &-md {
          }
        }
        &-input {
          width: 24rem;
          font-size: 2rem;
          &::placeholder {
          }
          :not(:last-child) {
          }
          &:focus {
          }
        }
        &-textarea {
          width: 48rem;
          font-size: 1.9rem;
          height: 10.5rem;
          &::placeholder {
          }

          &:focus {
          }
        }
        &-button {
          &:hover {
          }
        }
        &-arrow_right {
          transform: scaleX(1);
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    margin-top: 27rem;
    .footer {
      padding-top: 19rem;
      &-row {
      }
      &-column {
        &-left {
          padding: 4.25rem 0 1rem 0;
          &-logo {
            height: 11.5rem;
          }
        }
        &-right {
          &-row {
            padding-left: 0;
          }
          &-link_container {
          }
          &-link {
            text-align: center;
            font-size: 2.5rem;
            &:not(:last-child) {
            }

            &:hover {
            }
          }
        }
      }
      &-mini {
        &-container {
        }
        &-link_container {
          display: flex;
        }
        &-link {
          margin-right: 2rem;
          cursor: pointer;

          &:hover {
          }
        }
        &-copyright {
          letter-spacing: 1px;
          font-size: 1.7rem;
        }
      }
      &-contact {
        top: -16rem;
        padding: 5rem 4rem 2rem 4rem;
        width: 95%;
        &-form {
          flex-direction: column;
        }
        &-form_container {
          margin: 0 3rem;
          width: 100%;
          display: flex;
          justify-content: space-between;
          :not(:last-child) {
          }

          &-md {
            justify-content: right;
          }
        }
        &-input {
          width: 38rem;
          font-size: 2.1rem;
          margin-bottom: 2rem;
          &::placeholder {
          }
          :not(:last-child) {
            margin-right: 2rem;
            /* margin-bottom: 2rem; */
          }
          &:focus {
          }
        }
        &-textarea {
          width: 100%;
          font-size: 2.1rem;
          height: 13rem;
          margin-bottom: 2rem;
          &::placeholder {
          }

          &:focus {
          }
        }
        &-button {
          background-color: #fbfcff59;
          border-radius: 50%;
          display: flex;
          align-items: center;
          justify-content: center;
          padding: 1rem;
          &:hover {
          }
        }
        &-arrow_right {
          font-size: 5rem;
          font-weight: 900;
          transform: scaleX(1.1);
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    margin-top: 30rem;
    .footer {
      &-row {
      }
      &-column {
        &-left {
          padding: 6.4rem 0 1.35rem 0;
          &-logo {
            height: 11.5rem;
          }
        }
        &-right {
          &-row {
          }
          &-link_container {
          }
          &-link {
            font-size: 2.2rem;
            &:not(:last-child) {
            }

            &:hover {
            }
          }
        }
      }
      &-mini {
        &-container {
        }
        &-link_container {
          display: flex;
        }
        &-link {
          margin-right: 1rem;
          padding: 1rem;
          border-radius: 50%;
          background-color: ${color.primary_dark};
          color: ${color.primary_extra_light};
          box-shadow: 1px 3px 5px ${color.black};
          font-size: 2rem;

          &:hover {
          }
        }
        &-copyright {
        }
      }
      &-contact {
        width: 90%;
        top: -21rem;
        &-form {
        }
        &-form_container {
          display: block;

          :not(:last-child) {
          }

          &-md {
            display: flex;
            justify-content: right;
          }
        }
        &-input {
          width: 100%;
          &::placeholder {
          }
          :not(:last-child) {
          }
          &:focus {
          }
        }
        &-textarea {
          &::placeholder {
          }

          &:focus {
          }
        }
        &-button {
          &:hover {
          }
        }
        &-arrow_right {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    margin-top: 27rem;
    .footer {
      &-row {
      }
      &-column {
        &-left {
          padding: 5.8rem 0 0.4rem 0;
          &-logo {
            height: 8rem;
          }
        }
        &-right {
          &-row {
          }
          &-link_container {
          }
          &-link {
            font-size: 1.8rem;
            &:not(:last-child) {
            }

            &:hover {
            }
          }
        }
      }
      &-mini {
        &-container {
          display: block;
        }
        &-link_container {
          margin-bottom: 1.5rem;
          justify-content: center;
        }
        &-link {
          padding: 1.5rem;
          font-size: 2.5rem;

          &:hover {
          }
        }
        &-copyright {
          letter-spacing: 2px;
          display: flex;
          justify-content: center;
        }
      }
      &-contact {
        top: -19rem;
        &-form {
        }
        &-form_container {
          :not(:last-child) {
          }

          &-md {
          }
        }
        &-input {
          font-size: 1.8rem;
          &::placeholder {
          }
          :not(:last-child) {
          }
          &:focus {
          }
        }
        &-textarea {
          font-size: 1.8rem;
          &::placeholder {
          }

          &:focus {
          }
        }
        &-button {
          &:hover {
          }
        }
        &-arrow_right {
          font-size: 4rem;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
    .footer {
      &-row {
      }
      &-column {
        &-left {
          &-logo {
            height: 7rem;
          }
        }
        &-right {
          &-row {
          }
          &-link_container {
          }
          &-link {
            font-size: 1.6rem;
            &:not(:last-child) {
            }

            &:hover {
            }
          }
        }
      }
      &-mini {
        &-container {
        }
        &-link_container {
        }
        &-link {
          cursor: pointer;

          &:hover {
          }
        }
        &-copyright {
        }
      }
      &-contact {
        padding: 5rem 2.5rem 2rem 2.5rem;
        &-form {
        }
        &-form_container {
          :not(:last-child) {
          }

          &-md {
          }
        }
        &-input {
          font-size: 1.6rem;
          &::placeholder {
          }
          :not(:last-child) {
          }
          &:focus {
          }
        }
        &-textarea {
          font-size: 1.6rem;
          &::placeholder {
          }

          &:focus {
          }
        }
        &-button {
          &:hover {
          }
        }
        &-arrow_right {
        }
      }
    }
  }
`;

export default StyledFooter;
