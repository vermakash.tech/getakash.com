import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledWorkflow = styled.div`
  margin-top: 15rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  h1 {
    width: 100%;
    text-align: center;
    font-family: ${font.heading};
    font-size: 5rem;
    font-weight: 700;
    letter-spacing: -1px;
    color: ${color.black};
    margin-bottom: 8rem;
    text-shadow: 0 1px 2px #9b9da3;
  }
  img {
    width: 80%;
  }
  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
    h1 {
      font-size: 4rem;
      margin-bottom: 5rem;
    }
    img {
      width: 90%;
    }
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
    h1 {
      font-size: 3.5rem;
    }
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    h1 {
      font-size: 3rem;
    }
  }
`;

export default StyledWorkflow;
