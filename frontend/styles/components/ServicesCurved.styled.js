import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledServicesCurved = styled.div`
  .services_curved {
    &-row {
      height: 35rem;
    }
    &-text_col {
      padding: 4rem 6rem;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      h1 {
        color: ${color.primary};
        font-size: 2.75rem;
        text-align: center;
        font-family: ${font.heading};
        margin-bottom: 3rem;
        font-weight: 700;
      }
      h3 {
        font-size: 1.85rem;
        text-align: justify;
        line-height: 1.4;
      }
    }
    &-img_col {
      background-position: center;
      background-size: cover;
      position: relative;
      &-left {
        mask-image: url("/images/curv_serv_left.svg");
        mask-size: cover;
        mask-position: right;
        background-image: linear-gradient(to left, rgba(98, 55, 160, 75%), rgba(255, 255, 255, 1%)),
          url(${(props) => props.image});
      }
      &-right {
        mask-image: url("/images/curv_serv_right.svg");
        mask-size: cover;
        background-image: linear-gradient(to right, rgba(98, 55, 160, 75%), rgba(255, 255, 255, 1%)),
          url(${(props) => props.image});
      }
    }
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
    .services_curved {
      &-row {
        height: 30rem;
      }
      &-text_col {
        padding: 3rem 5rem;
        h1 {
          font-size: 2.75rem;
          margin-bottom: 2rem;
        }
        h3 {
          font-size: 1.75rem;
          text-align: justify;
          line-height: 1.2;
        }
      }
      &-img_col {
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .services_curved {
      &-row {
        height: 23.5rem;
      }
      &-text_col {
        padding: 1rem 2rem;
        h1 {
          font-size: 2.25rem;
          margin-bottom: 2rem;
        }
        h3 {
          font-size: 1.5rem;
          text-align: justify;
          line-height: 1.1;
        }
      }
      &-img_col {
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .services_curved {
      &-row {
        height: 25rem;
        margin-bottom: 1rem;
      }
      &-text_col {
        /* display: flex;
        flex-direction: column;
        justify-content: center; */
        align-items: flex-start;
        h1 {
          font-size: 2.45rem;
          margin-bottom: 1rem;
          text-align: left;
        }
        h3 {
          font-size: 1.65rem;
          text-align: left;
          line-height: 1.1;
        }
      }
      &-img_col {
        background-position: center;
        &-right {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .services_curved {
      &-row {
      }
      &-text_col {
        h1 {
        }
        h3 {
          line-height: 1;
        }
      }
      &-img_col {
      }
    }
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .services_curved {
      &-row {
      }
      &-text_col {
        h1 {
          font-size: 2rem;
        }
        h3 {
          line-height: 1;
        }
      }
      &-img_col {
      }
    }
  }
`;

export default StyledServicesCurved;
