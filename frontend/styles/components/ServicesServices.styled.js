import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledServicesServices = styled.div`
  margin-top: 10rem;
  padding: 0 10rem;

  .services_services {
    &-heading {
      font-size: 3.5rem;
      text-align: center;
      font-family: ${font.heading};
      font-weight: 800;
      margin: 4rem 0 7rem 0;
    }
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
    padding: 0 5rem;
  }

  @media only screen and (max-width: ${bp.large}) {
    margin-top: 10rem;
    padding: 0 1rem;
  }

  @media only screen and (max-width: ${bp.medium}) {
    margin-top: 10rem;
    padding: 0 0;
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
  }
  @media only screen and (max-width: ${bp.xx_small}) {
  }
`;

export default StyledServicesServices;
