import styled from 'styled-components'
import {font, color, bp} from '../variables'

const StyledLanding = styled.div`
  width: 100vw;
  padding-top: 3rem;
  padding-bottom: 2rem;
  background-color: ${color.primary_dark};
  position: relative;

  .bg_img {
    position: absolute;
    width: 100%;
    z-index: 100;

    &-sm {
      top: 15rem;
    }
    &-lg {
      &-1 {
        top: 20rem;
      }
      &-2 {
        bottom: 5rem;
      }
      &-3 {
        top: 26rem;
        display: none;
      }
    }
  }

  .landing {
    /* background-color: pink; */
    padding-top: 5rem;
    position: relative;
    z-index: 900;

    &-left {
      /* background-color: pink; */
      display: flex;
      flex-direction: column;
      justify-content: center;

      &-tag {
        font-family: ${font.heading};
        font-weight: 700;
        font-size: 4.2rem;
        line-height: 1;
        color: ${color.white};
      }
      &-desc {
        font-family: ${font.primary};
        font-size: 1.9rem;
        font-weight: 300;
        color: ${color.white};
        margin: 3rem auto 4.5rem auto;
      }
      &-cta_button {
        width: 40rem;
        background-color: ${color.secondary_light};
        font-size: 3.5rem;
        font-family: ${font.heading};
        font-weight: 700;
        padding: 2.25rem 2rem;
        line-height: 1;
        border: none;
        border-radius: 2rem;
        color: ${color.secondary_dark};
        transition: all 0.1s;

        &:hover {
          border: none;
          transform: translateY(-4px);
          box-shadow: 0.5rem 1rem 1rem ${color.black};
        }
        &:active {
          transform: translateY(-4px);
          transform: translateY(0);
          box-shadow: 0.3rem 0.6rem 0.8rem ${color.black};
        }
      }
    }
    &-right {
      display: flex;
      justify-content: center;
      align-items: center;
      /* background-color: green; */
      padding: 1rem 0;
      padding-left: 2rem;

      &-image {
        transform: scale(0.82);
      }
    }

    .contact_link {
      cursor: pointer;
    }
    .monitor_button {
      cursor: pointer;
    }
    .tablet_button {
      cursor: pointer;
    }
    .tablet_off_screen {
      display: none;
    }
    .monitor_off_screen {
      display: none;
    }
  }

  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
    width: 100vw;
    padding-top: 3rem;
    padding-bottom: 2rem;
    .landing {
      &-left {
      }
      &-right {
        &-image {
          transform: scale(0.72);
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.large}) {
    padding-top: 12rem;
    padding-bottom: 10rem;

    .landing {
      &-left {
        align-items: center;
        padding: 0 9rem;

        &-tag {
          text-align: center;
        }
        &-desc {
          text-align: center;
          margin: 6rem auto 8rem auto;
        }
        &-cta_button {
        }
      }
      &-right {
        display: none;
        &-image {
          display: none;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .bg_img {
      &-sm {
      }
      &-lg {
        &-1 {
        }
        &-2 {
        }
        &-3 {
          display: block;
        }
      }
    }

    .landing {
      &-left {
        &-tag {
        }
        &-desc {
        }
        &-cta_button {
        }
      }
      &-right {
        &-image {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .landing {
      &-left {
        padding: 0 5rem;

        &-tag {
        }
        &-desc {
        }
        &-cta_button {
        }
      }
      &-right {
        &-image {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    padding-top: 10rem;

    .landing {
      &-left {
        padding: 0 2rem;

        &-tag {
        }
        &-desc {
        }
        &-cta_button {
          width: 35rem;
        }
      }
      &-right {
        &-image {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
    padding-top: 8rem;
    .landing {
      &-left {
        padding: 0 2rem;

        &-tag {
        }
        &-desc {
          margin: 2.5rem auto 4rem auto;
        }
        &-cta_button {
          font-size: 3rem;
          padding: 2.25rem 2rem;
          width: 30rem;
        }
      }
      &-right {
        &-image {
        }
      }
    }
  }
`;

export default StyledLanding