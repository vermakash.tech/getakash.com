import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledResume = styled.div`
  margin-top: 10rem;
  // margin: 3rem;
  padding: 4rem 4rem 0 4rem;
  grid-gap: 2rem !important;
  .col-lg-5 {
    // background-color: cyan;
    // height: 15rem;
    padding: 2rem;
  }
  .col-lg-10 {
    // background-color: aquamarine;
    padding: 2rem;
    // margin-right: 4rem;
  }
  .resume_about {
    &-certi_img {
      position: fixed;
      top: 0;
      left: 0;
      width: 100vw;
      height: 100vh;
      background-color: ${`${color.grey}E6`};
      backdrop-filter: blur(2px);
      z-index: 5000;
      &-cross {
        position: absolute;
        right: 4rem;
        top: 3rem;
        font-size: 4rem;
        color: ${color.white_dark};
        cursor: pointer;
      }
      &-img {
        width: 55%;
        position: absolute;
        top: 50%;
        left: 50%;
        box-shadow: 0.3rem 0.8rem 1.5rem ${color.primary_dark};
        transform: translate(-50%, -50%);
        animation: pop_out_certi 0.2s ease-out;
      }
    }
    &-row {
      justify-content: center !important;
    }
    &-col {
      /* justify-content: center !important; */
      /* margin: 0 auto; */
      display: flex;
      flex-direction: column;
      &:not(:last-child) {
        margin-bottom: 3rem;
      }
    }
    &-edex {
      &--item {
        /* box-shadow: 0 0.6rem 1rem ${color.grey_light}; */
        position: relative;
        background-color: ${color.white_dark};
        width: 100%;
        height: 100%;
        border-radius: 2px;
        h1 {
          padding: 1.5rem;
          font-size: 2.4rem;
          font-weight: 700;
          border-radius: 2px;
          color: ${color.white};
          background-color: ${color.primary};
          display: inline-block;
          position: absolute;
          left: -1rem;
          top: -1rem;
        }
      }
      &--ex {
        padding: 7rem 2rem 2rem 2rem;
        .ex {
          margin-bottom: 3rem;
          &-intro {
            display: flex;
            align-items: center;
            img {
              width: 5rem;
              border: none;
            }
            h3 {
              margin: 0;
              font-size: 1.9rem;
              font-weight: 700;
            }
            div {
              overflow: hidden;
              margin-left: 1rem;
              margin-top: 0.5rem;
              display: block;
              h4 {
                font-size: 1.8rem;
                font-weight: 500;
                span:not(:last-child) {
                  word-wrap: break-word;
                  white-space: normal;
                  padding-right: 0.5rem;
                  margin-right: 0.5rem;
                  border-right: 1px solid ${color.black};
                }
              }
            }
          }
          &-desc {
            padding: 1rem 0 0 3rem;
            ul {
              font-size: 1.7rem;
            }
          }
        }
      }
      &--ed {
        padding: 7rem 2rem 2rem 3rem;
        margin-bottom: 5rem;
        .ed {
          margin-bottom: 2rem;
          &:not(:first-of-type) {
            margin-top: 3rem;
          }
          &-intro {
            display: flex;
            align-items: center;
            img {
              width: 5rem;
              border: none;
            }
            h3 {
              font-size: 1.9rem;
              font-weight: 700;
            }
            div {
              margin-left: 1rem;
              margin-top: 0.5rem;
              display: block;
              h4 {
                font-size: 1.8rem;
                font-weight: 500;
                span:not(:last-child) {
                  padding-right: 0.5rem;
                  margin-right: 0.5rem;
                  border-right: 1px solid ${color.black};
                }
              }
            }
          }
          &-desc {
            padding: 1rem 0 0 3rem;
            ul {
              font-size: 1.7rem;
            }
          }
        }
      }
      &--cert {
        padding: 7rem 2rem 2rem 2rem;
        display: flex;
        @media screen and (max-width: 22em) {
          flex-direction: column;
        }
        .cert {
          padding-left: 2rem;
          font-size: 1.25rem;
          margin-right: 1rem;
          a {
            text-decoration: none;
            font-weight: 600;
          }
          li {
            font-size: 1.75rem;
            display: flex;
            align-items: center;
            transition: all 0.1s;
            margin-bottom: 1rem;
            cursor: pointer;
            .fileIcon {
              font-size: 2.5rem;
              color: ${color.primary};
              margin-right: 1rem;
              transition: all 0.1s;
            }
            &:hover {
              text-decoration: underline;
              .fileIcon {
                transform: scale(1.1);
              }
            }
            @media screen and (max-width: 26em) {
              margin-bottom: 2rem;
            }
          }
        }
      }
      &--excurr {
        padding: 7rem 2rem 2rem 2rem;
        .excurr {
          display: flex;
          align-items: center;
          padding-left: 2rem;
          font-size: 1.7rem;
          img {
            width: 5rem;
            margin-right: 0.5rem;
          }
          li {
            list-style: none;
            span {
              font-size: 1.65rem;
              font-weight: 600;
            }
          }
        }
      }
      &--hob {
        padding: 7rem 2rem 2rem 2rem;
        .hob {
          display: flex;
          padding-left: 2rem;
          font-size: 1.7rem;
          @media screen and (max-width: 21em) {
            flex-direction: column;
          }
          div {
            display: flex;
            justify-content: space-around;
            @media screen and (max-width: 21em) {
              margin-bottom: 1.5rem;
            }
          }
          li {
            list-style: none;
            margin-right: 4rem;
            @media screen and (max-width: 21em) {
              display: flex;
              flex-direction: column;
              margin-bottom: 1.5rem;
            }
            img {
              width: 4rem;
              margin-right: 0.5rem;
            }
          }
          // @media screen and (max-width: 20em){
          //     flex-direction: column;
          // }
        }
        @media screen and (max-width: 20em) {
          padding: 7rem 3rem 2rem 1rem;
        }
      }
    }
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
    .resume_about {
      &-certi_img {
        &-cross {
          font-size: 4.5rem;
        }
        &-img {
          width: 70%;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
    .resume_about {
      &-certi_img {
        &-cross {
          font-size: 5rem;
        }
        &-img {
          width: 80%;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    margin-top: 10rem;
    // margin: 3rem;
    padding: 2.5rem;
    grid-gap: 2rem !important;
    .col-lg-5 {
      // background-color: cyan;
      // height: 15rem;
      padding: 0;
    }
    .col-lg-10 {
      // background-color: aquamarine;
      padding: 0;
      // margin-right: 4rem;
    }
    .resume_about {
      &-edex {
        &--hob {
          .hob {
            flex-direction: column;
          }
        }
      }
      &-certi_img {
        &-cross {
          font-size: 5rem;
        }
        &-img {
          width: 90%;
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .resume_about {
      &-edex {
        &--item {
          h1 {
            font-size: 2.3rem;
          }
        }
        &--ex {
          .ex {
            &-intro {
              h3 {
                font-size: 1.7rem;
              }
              div {
                h4 {
                  font-size: 1.7rem;
                }
              }
            }
            &-desc {
              ul {
                font-size: 1.6rem;
              }
            }
          }
        }
        &--ed {
          .ed {
            &-intro {
              h3 {
                font-size: 1.7rem;
                font-weight: 700;
              }
              div {
                h4 {
                  font-size: 1.7rem;
                }
              }
            }
            &-desc {
              padding: 1rem 0 0 3rem;
              ul {
                font-size: 1.6rem;
              }
            }
          }
        }
        &--cert {
          .cert {
            font-size: 1.2rem;
            li {
              font-size: 1.65rem;
              cursor: pointer;
              .fileIcon {
              }
            }
          }
        }
      }
    }
  }
`;

export default StyledResume;
