import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledLandingAbout = styled.div`
  margin-bottom: 10rem;
  .landing_about {
    &-row {
    }
    &-col {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      img {
        animation: fade_in 0.5s ease;
        width: 95%;
      }
    }
    &-desc {
      &-primary {
        font-size: 1.75rem;
        line-height: 1.2;
        text-align: justify;
        margin-bottom: 2.5rem;
        align-self: flex-start;
        color: ${color.black};
        span:first-child {
          display: block;
          font-size: 3.5rem;
          margin-bottom: 0.5rem;
          color: ${color.primary_light};
          font-weight: 700;
        }
        span {
          font-size: 1.8rem;
          color: ${color.primary_light};
          font-weight: 900;
        }
      }
      &-secondary {
        font-size: 1.75rem;
        line-height: 1.2;
        text-align: justify;
        margin-bottom: 2.5rem;
        align-self: flex-start;
        color: ${color.black};
        span {
          font-size: 1.8rem;
          color: ${color.primary_light};
          font-weight: 900;
        }
      }
      &-link_container {
        align-self: flex-start;
        padding: 1rem 0 1rem 0.5rem;
        a {
          &:not(:last-child) {
            margin-right: 2.5rem;
          }
        }
      }
      &-link {
        width: 2.5rem;
        height: 2.5rem;
        padding: 1.25rem;
        border-radius: 50%;
        background-color: ${color.primary};
        color: ${color.white_dark};
        box-shadow: 0.25rem 0.5rem 0.5rem ${color.grey_light};
        cursor: pointer;
        transition: all 0.1s;
        /* &:not(:last-child) {
          margin-right: 2.5rem;
        } */
        &:hover {
          transform: scale(1.05);
        }
      }
    }
  }
  @keyframes name {
  }

  @media only screen and (min-width: ${bp.x_largest}) {
    .landing_about {
      &-row {
      }
      &-col {
        img {
          width: 90%;
        }
      }
      &-desc {
        &-primary {
          font-size: 1.85rem;
          line-height: 1.3;
          text-align: justify;
          width: 80%;
          margin-bottom: 3.5rem;
          span:first-child {
            font-size: 4rem;
          }
          span {
            font-size: 1.9rem;
          }
        }
        &-secondary {
          font-size: 1.85rem;
          line-height: 1.3;
          text-align: justify;
          width: 80%;
          margin-bottom: 3.5rem;
          span {
            font-size: 1.9rem;
          }
        }
        &-link_container {
          padding: 1rem 0 1rem 0.5rem;
        }
        &-link {
        }
      }
    }
  }
  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
    .landing_about {
      &-row {
      }
      &-col {
        img {
        }
      }
      &-desc {
        margin-top: 4rem;
        &-primary {
          font-size: 2rem;
          line-height: 1.2;
          text-align: justify;
          width: 90%;
          margin-bottom: 3.5rem;
          align-self: auto;
          span:first-child {
            font-size: 4rem;
          }
          span {
            font-size: 2.1rem;
          }
        }
        &-secondary {
          font-size: 2rem;
          line-height: 1.2;
          text-align: justify;
          width: 90%;
          margin-bottom: 3.5rem;
          align-self: auto;
          span {
            font-size: 2.1rem;
          }
        }
        &-link_container {
          padding: 1rem 0 1rem 0.5rem;
          align-self: auto;
        }
        &-link {
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .landing_about {
      &-row {
      }
      &-col {
        img {
          width: 100%;
        }
      }
      &-desc {
        margin-top: 4rem;
        &-primary {
          width: 95%;
          margin-bottom: 2.5rem;
          span:first-child {
            font-size: 4rem;
          }
          span {
            font-size: 2.1rem;
          }
        }
        &-secondary {
          font-size: 2rem;
          width: 95%;
          margin-bottom: 3.5rem;
          span {
            font-size: 2.1rem;
          }
        }
        &-link_container {
        }
        &-link {
          width: 2.5rem;
          height: 2.5rem;
          padding: 1.25rem;
          border-radius: 50%;
          background-color: ${color.primary};
          color: ${color.white_dark};
          box-shadow: 0.25rem 0.5rem 0.5rem ${color.grey_light};
          cursor: pointer;
          transition: all 0.1s;
          &:not(:last-child) {
            margin-right: 1.5rem;
          }
        }
      }
    }
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .landing_about {
      &-desc {
        &-link_container {
        }
        &-link {
        }
      }
    }
  }
`;

export default StyledLandingAbout;
