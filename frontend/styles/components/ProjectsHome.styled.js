import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledProjectsHome = styled.div`
  margin-top: 10rem;
  margin-bottom: 14.5rem;
  .projects_home {
    height: 100%;
    &-heading {
      width: 100%;
      text-align: center;
      font-family: ${font.heading};
      font-size: 5rem;
      font-weight: 700;
      letter-spacing: -1px;
      color: ${color.black};
      margin-bottom: 6rem;
      text-shadow: 0 1px 2px #9b9da3;
      &-redirect {
        cursor: pointer;
        transform: scale(0.9);
        transition: all 0.1s;
        &:hover {
          color: ${color.primary};
          transform: scale(0.95);
        }
      }
    }
    &-reduced_card {
      height: 100%;
      overflow: hidden;
      cursor: pointer;
      position: relative;

      &-content {
        position: absolute;
        text-align: left;
        width: 40rem;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(-90deg);
        font-size: 3rem;
        font-weight: 700;
        font-family: ${font.heading};
        color: ${color.primary_dark};
        letter-spacing: 2px;
        transition: all 0.1s;
      }
      &:hover {
        .projects_home-reduced_card-content {
          transform: translate(-50%, -50%) rotate(-90deg) scale(1.05);
        }
      }
    }
    &-container {
      margin: 0 17.5rem 4rem 17.5rem;
      display: flex;
      /* box-shadow: 0 0 0 10px hsl(280, 52%, 70%), 0 0 0 15px hsl(280, 52%, 80%), 0 0 0 20px hsl(280, 52%, 90%); */
    }
  }
  #proj-1 {
    background-color: #deacf5;
  }
  #proj-2 {
    background-color: #c084db;
  }
  #proj-3 {
    background-color: #9f5ebd;
  }
  #proj-4 {
    background-color: #9653b5;
  }
  #proj-5 {
    background-color: #8a44ab;
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
    .projects_home {
      &-heading {
      }
      &-reduced_card {
        &-content {
        }
      }
      &-container {
        margin: 0 10rem 4rem 10rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.large}) {
    margin-bottom: 13.5rem;
    .projects_home {
      &-heading {
      }
      &-reduced_card {
        &-content {
          font-size: 3rem;
        }
      }
      &-container {
        margin: 0 0rem 4rem 0rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .projects_home {
      &-heading {
        font-size: 4rem;
      }
      &-reduced_card {
        &-content {
          font-size: 2.5rem;
          font-weight: 800;
        }
      }
      &-container {
        margin: 0 0rem 4rem 0rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .projects_home {
      &-heading {
        margin-bottom: 5rem;
      }
      &-reduced_card {
        height: 7rem;
        width: 40rem;
        /* overflow: hidden; */
        cursor: pointer;
        position: relative;

        &-content {
          margin-left: 2rem;
          position: absolute;
          text-align: left;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          font-size: 3rem;
          font-weight: 700;
          font-family: ${font.heading};
          color: ${color.black};
          letter-spacing: 2px;
          transition: all 0.1s;
        }
        &:hover {
          .projects_home-reduced_card-content {
            transform: translate(-50%, -50%) scale(1.05);
          }
        }
      }
      &-container {
        display: flex;
        flex-direction: column;
        align-items: center;
        box-shadow: none;
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .projects_home {
      &-heading {
        font-size: 3.5rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
    .projects_home {
      &-heading {
        font-size: 3rem;
      }
      &-reduced_card {
        width: 32rem;

        &-content {
          margin-left: 1.5rem;
          position: absolute;
          text-align: left;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          font-size: 2.7rem;
          width: 30rem;
        }
      }
    }
  }
`;

export default StyledProjectsHome;
