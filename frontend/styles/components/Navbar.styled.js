import styled from 'styled-components'
import {font, color, bp} from '../variables'

const StyledNavbar = styled.nav`
  width: 100vw;
  min-height: 9rem;
  background-color: ${color.primary_dark};
  position: fixed;
  top: 0;
  border-bottom: ${(props) => (props.isMobile ? `2px solid ${color.primary_light}` : "none")};
  z-index: 1000;
  animation: fade_in 1s ease;
  .navbar {
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: ${color.white};
    height: inherit;
    padding-top: 1.8rem;
    padding-bottom: 1.8rem;

    &_logo {
      cursor: pointer;
      &_img {
        height: 6rem;
      }
    }
    &_container {
      display: flex;
      align-items: center;

      &_links {
        display: flex;
        align-items: center;
      }
    }
    &_link {
      font-size: 2rem;
      font-weight: 700;
      color: ${color.primary_extra_light};
      margin-right: 2rem;
      text-decoration: none;
      transition: all 0.1s;
      border-bottom: 2.5px solid transparent;

      &:hover {
        border-bottom: 2.5px solid ${color.primary_extra_light};
      }
    }
    &_link-active {
      border-bottom: 2.5px solid ${color.primary_extra_light};
    }
    &_cta {
      font-size: 2rem;
      font-weight: 700;
      border-radius: 1rem;
      padding: 0.5rem 1.8rem;
      text-align: center;
      text-decoration: none;
      background-color: ${color.primary_extra_light};
      color: ${color.primary};
      transition: all 0.1s;
      cursor: pointer;

      &:hover {
        transform: translateY(-2px);
      }
      &:active {
        transform: translateY(-2px);
        transform: translateY(0);
      }
    }
    &_hamburger {
      font-size: 4rem;
      margin-left: 1rem;
      width: 4rem;
      color: ${color.primary_extra_light};
      cursor: pointer;
    }
    &_cross {
      font-size: 4.5rem;
    }
  }

  @media only screen and (max-width: ${bp.large}) {
    .navbar {
      &_container {
        &_links {
          overflow-y: hidden;
          flex-direction: column;
          justify-content: space-around;
          align-items: center;
          max-height: 40rem;
          width: 100vw;
          transition: all 0.2s ease;
          padding-top: 0.8rem;

          &_closed {
            max-height: 0;
            padding: 0;
          }
        }
      }
      &_link {
        font-size: 2.1rem;
        margin: 0.6rem auto;
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .navbar {
      &_link {
        font-size: 2.3rem;
        margin: 0.7rem auto;
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .navbar {
      &_link {
        font-size: 2.3rem;
        margin: 0.7rem auto;
      }
      &_hamburger {
        margin-left: 0.5rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    min-height: 7rem;
    .navbar {
      &_logo {
        &_img {
          height: 4.5rem;
        }
      }
      &_link {
        font-size: 2rem;
        margin: 0.5rem auto;
      }
      &_hamburger {
        margin-left: 0.4rem;
      }
      &_cta {
        padding: 0.5rem 1.5rem;
        font-size: 1.6rem;
      }
    }
  }

  @media only screen and (max-width: ${bp.xx_small}) {
    min-height: 6rem;
    .navbar {
      &_logo {
        &_img {
          height: 4rem;
        }
      }
      &_link {
        font-size: 2rem;
        margin: 0.5rem auto;
      }
      &_hamburger {
        width: 3.6rem;
        font-size: 3.4rem;
        margin-left: 0.2rem;
      }
      &_cross {
        font-size: 3.8rem;
      }
      &_cta {
        padding: 0.5rem 1.1rem;
        font-size: 1.4rem;
      }
    }
  }
`;

export default StyledNavbar