import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledLandingServices = styled.div`
  .landing_services {
    &-row {
      justify-content: center;
    }
    &-img_container {
      display: flex;
      align-items: center;
      img {
        width: 100%;
      }
    }
    &-content {
      display: flex;
      flex-direction: column;
      justify-content: center;
      &-heading {
        color: ${color.primary};
        font-weight: 700;
        font-size: 3.25rem;
        margin-bottom: 3rem;
        font-family: ${font.heading};

        &-primary {
          color: ${color.black};
          font-weight: 600;
          font-size: 2.75rem;
        }
      }
      &-desc {
        font-weight: 300;
        font-size: 2.25rem;
        &-primary {
          margin-bottom: 2px;
          display: block;
          font-weight: 700;
          font-size: 2rem;
        }
      }
    }
  }

  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
    .landing_services {
      &-row {
        /* margin: 0 1rem; */
      }
      &-img_container {
        img {
        }
      }
      &-content {
        &-heading {
          font-size: 3rem;
          margin-bottom: 2.5rem;

          &-primary {
            font-size: 2.5rem;
          }
        }
        &-desc {
          font-weight: 300;
          font-size: 2rem;
          &-primary {
            font-weight: 700;
            font-size: 1.75rem;
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.medium}) {
    .landing_services {
      &-row {
        /* margin: 0 1rem; */
      }
      &-img_container {
        margin-bottom: 5rem;
        justify-content: center;
        img {
          width: 85%;
        }
      }
      &-content {
        width: 85%;
        &-heading {
          line-height: 1;
          text-align: center;
          font-size: 3.5rem;
          margin-bottom: 3.5rem;

          &-primary {
            font-size: 3rem;
          }
        }
        &-desc {
          text-align: center;
          font-weight: 300;
          font-size: 2.5rem;
          &-primary {
            font-weight: 700;
            font-size: 2.25rem;
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
    .landing_services {
      &-row {
        /* margin: 0 1rem; */
      }
      &-img_container {
        img {
          width: 90%;
        }
      }
      &-content {
        width: 90%;
        &-heading {
          &-primary {
          }
        }
        &-desc {
          &-primary {
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.x_small}) {
    /* margin-top: 14rem; */
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    /* margin-top: 12rem; */
  }
`;

export default StyledLandingServices;
