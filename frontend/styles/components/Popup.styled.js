import styled from "styled-components";
import { font, color, bp } from "../variables";
// ${(props) => (props.type == "SUCCESS" ? color.primary : color.grey)}

const StyledPopup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: ${`${color.grey}E6`};
  backdrop-filter: blur(2px);
  z-index: 5000;
  .popup {
    animation: fromTop 0.2s ease-out;
    border-radius: 0.5rem;
    position: absolute;
    padding: 9rem 2rem 4rem 2rem;
    width: 42.5rem;
    top: 12.5%;
    left: 50%;
    transform: translateX(-50%);
    background-color: ${color.white};
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    &-message {
      text-align: center;
      color: ${color.grey};
      font-size: 2rem;
      margin-bottom: 2.5rem;
    }
    &-icon {
      font-size: 2rem;
      width: 12rem;
      height: 12rem;
      background-color: ${(props) =>
        props.type == "SUCCESS" || props.type == "THANK YOU" ? color.primary : "#b02020"};
      border-radius: 50%;
      padding: 1.5rem;
      color: ${color.white};
      position: absolute;
      top: -6rem;
      left: 50%;
      transform: translateX(-50%);
    }
    &-type {
      color: ${(props) => (props.type == "SUCCESS" || props.type == "THANK YOU" ? color.grey : color.black)};
      font-weight: 700;
      font-family: ${font.heading};
      margin-bottom: 1rem;
      letter-spacing: 0.9;
      font-size: 3.5rem;
    }
    &-close {
      width: 90%;
      background-color: ${(props) =>
        props.type == "SUCCESS" || props.type == "THANK YOU" ? color.primary : "#b02020"};
      padding: 0.5rem;
      text-align: center;
      font-size: 2.5rem;
      color: ${color.white};
      border: none;
      outline: none;
      border-radius: 0.5rem;
      transition: all 0.2s;
      &:hover {
        transform: translateY(-1.5px);
      }
    }
  }
  @keyframes name {
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .popup {
      width: 32.5rem;
    }
  }
`;

export default StyledPopup;
