import { createGlobalStyle } from 'styled-components'
import {font, color, bp} from './variables'

const UtilityStyle = createGlobalStyle`
    .my-container{
        margin: 0 10rem; // #container of 10 rem
    }
    .bold{
        font-weight: 700;
    }
    .page_link{
        text-decoration: underline;
        cursor: pointer;
        &:hover{
            color: ${color.primary}
        }
    }

    @keyframes fade_in {
     0%{
        opacity: 0;
     }   
     50%{
        opacity: 0;
     }
     100%{
        opacity: 100;
     }
    }
    @keyframes fromTop {
     0%{
        transform: translateX(-50%) translateY(-5rem);
     }   
     100%{
        transform: translateX(-50%) translateY(0);
     }
    }
    
    @keyframes pop_out {
     0%{
        opacity: 0;
        transform: scale(0.7);
     }   
     100%{
        opacity: 100;
        transform: scale(1);
     }
    }
    @keyframes pop_out_certi {
     0%{
        opacity: 0;
        transform: translate(-50%, -50%) scale(0.7);
     }   
     100%{
        opacity: 100;
        transform: translate(-50%, -50%) scale(1);
     }
    }
    @keyframes shake {
     0%{
        transform: translateX(0);
     }   
     25%{
        transform: translateX(-2rem);
     }   
     50%{
        transform: translateX(0);
     }   
     75%{
        transform: translateX(2rem);
     }   
     100%{
        transform: translateX(0);
     }
    }

    @keyframes roll_down {
        0%{
            max-height: 0;
        }
        100%{
            max-height: 40rem;
        }
    }
    @keyframes roll_up {
        0%{
            max-height: 40rem;
        }
        100%{
            max-height: 0;
        }
    }
    @keyframes rotate_90_from_right {
        0%{
            transform: rotate(-90deg);
            bottom: -13rem;
        }
        100%{
            transform: rotate(0);
            bottom: 0
        }
    }
    @keyframes rotate_90_from_left {
        0%{
            transform: rotate(-90deg);
            bottom: -13rem;
            right: 14rem;
        }
        100%{
            transform: rotate(0);
            bottom: 0;
            right: 2rem;
        }
    }
    @keyframes rotate_90_from_bottom {
        0%{
            transform: rotate(-90deg);
            bottom: -15rem;
            right: 12rem;
        }
        100%{
            transform: rotate(0);
            bottom: 0;
            right: 2rem;
        }
    }
    @keyframes rotate_90_from_top {
        0%{
            transform: rotate(-90deg);
            bottom: 13rem;
            right: 13rem;
        }
        100%{
            transform: rotate(0);
            bottom: 0;
            right: 2rem;
        }
    }

    @media only screen and (max-width: ${bp.large}) {
        
    }

    @media only screen and (max-width: ${bp.medium}) {
        .my-container{
            margin: 0 6rem; // #container of 6 rem
        }
    }

    @media only screen and (max-width: ${bp.small}) {
        .my-container{
            margin: 0 3rem; // #container of 3 rem
        }
    }
    
    @media only screen and (max-width: ${bp.x_small}) {
        
    }

    @media only screen and (max-width: ${bp.xx_small}) {
        .my-container{
            margin: 0 1rem; // #container of 1 rem
        }
    }
`;

export default UtilityStyle