import styled from 'styled-components'
import {font, color} from '../variables'

const StyledIndex = styled.div`
  animation: fade_in 1s ease;
  position: relative;
`;

export default StyledIndex