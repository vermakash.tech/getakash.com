import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledProjects = styled.div`
  position: relative;
  padding-top: 15rem;
  animation: fade_in 1s ease;
  .projects_page {
    &-heading {
      width: 100%;
      text-align: center;
      font-family: ${font.heading};
      font-size: 4.5rem;
      font-weight: 700;
      letter-spacing: -1px;
      color: ${color.black};
      margin-bottom: 5rem;
      text-shadow: 0 1px 2px #9b9da3;
    }
    &-filter {
      display: flex;
      justify-content: center;
      align-items: center;
      margin-bottom: 7rem;
      &-button {
        width: 27.5rem;
        padding: 0.5rem 0;
        font-size: 2.5rem;
        font-weight: 700;
        text-align: center;
        background-color: ${color.grey_light};
        color: ${color.black};
        cursor: pointer;
        /* border: 1px solid ${color.primary_dark}; */
        &-active {
          background-image: linear-gradient(to right, ${color.primary}, ${color.primary_dark}, ${color.primary});
          color: ${color.white};
          transform: scale(1.01);
        }
      }
    }
    &-row {
    }
    &-column {
      display: flex;
      align-items: center;
      justify-content: center;
      margin-bottom: 5rem;
    }
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
    .projects_page {
      &-heading {
        font-size: 4rem;
      }
      &-filter {
        &-button {
          width: 22.5rem;
          padding: 0.5rem 0;
          font-size: 2.2rem;
          &-active {
          }
        }
      }
    }
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
    .projects_page {
      &-heading {
        font-size: 3.5rem;
      }
    }
  }
  @media only screen and (max-width: ${bp.xx_small}) {
    .projects_page {
      &-heading {
        font-size: 3rem;
      }
    }
  }
`;

export default StyledProjects;
