import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledAbout = styled.div`
  padding-top: 17.5rem;
  animation: fade_in 1s ease;
  position: relative;

  @media only screen and (max-width: ${bp.x_small}) {
    padding-top: 14rem;
  }
`;

export default StyledAbout;
