import styled from "styled-components";
import { font, color } from "../variables";

const StyledServices = styled.div`
  padding-top: 17.5rem;
  animation: fade_in 1s ease;
  position: relative;
`;

export default StyledServices;
