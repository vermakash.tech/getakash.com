import styled from "styled-components";
import { font, color, bp } from "../variables";

const StyledAdmin = styled.div`
  padding-top: 17.5rem;
  animation: fade_in 1s ease;
  position: relative;
  width: 100vw;
  height: 100vh;
  .admin_container {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .admin_login {
    p {
      text-align: center;
      font-size: 1.5rem;
      font-size: 1.5rem;
      transition: all 0.2s;
    }
    &-hidden {
      visibility: hidden;
    }
    &-shake {
      animation: shake 0.2s ease-out;
    }
    form {
      font-size: 2rem;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      input {
        width: 40rem;
        border-radius: 1px;
        margin-bottom: 1rem;
        padding: 0.75rem 1.25rem;
        color: ${color.black};
        background-color: ${color.white_dark};
        outline: none;
        border: none;
        ::placeholder {
          color: ${color.grey_light};
        }
        &:hover {
          outline: none;
          border: none;
        }
      }
      textarea {
        width: 40rem;
        height: 20rem;
        border-radius: 1px;
        margin-bottom: 1rem;
        padding: 0.75rem 1.25rem;
        color: ${color.black};
        background-color: ${color.white_dark};
        outline: none;
        border: none;
        ::placeholder {
          color: ${color.grey_light};
        }
        &:hover {
          outline: none;
          border: none;
        }
      }
      button {
        border-radius: 2px;
        width: 25rem;
        margin-top: 1rem;
        font-size: 2.5rem;
        padding: 0.5rem 2rem;
        background-color: ${color.primary};
        color: ${color.white};
        border: none;
      }
    }
  }

  @media only screen and (max-width: ${bp.largest}) {
  }

  @media only screen and (max-width: ${bp.large}) {
  }

  @media only screen and (max-width: ${bp.medium}) {
  }

  @media only screen and (max-width: ${bp.small}) {
  }

  @media only screen and (max-width: ${bp.x_small}) {
    padding-top: 14rem;
  }

  @media only screen and (max-width: ${bp.xx_small}) {
  }
`;

export default StyledAdmin;
